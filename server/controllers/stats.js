const Contacts = require('../models/Contacts');
const Lists = require('../models/Lists');
const Logs = require('../models/Logs');
const Vendors = require('../models/Vendors');
const Templates = require('../models/Templates');

const getAll = async (req, res) => {
  try {
    const stats = {};
    stats.contacts = await Contacts.collection.count();
    stats.lists = await Lists.collection.count();
    stats.templates = await Templates.collection.count();
    stats.vendors = await Vendors.collection.count();
    stats.exports = await Logs.find()
      .sort({ _id: -1 })
      .select('-__v')
      .limit(10)
      .populate('list', '-_id -__v -created_at -template -vendor')
      .populate(
        'user',
        '_id -__v -vendors -createdDate -password -tokens -reset_password_token -phone -username'
      );

    res.status(200).json({ stats });
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
};

module.exports = {
  getAll,
};
