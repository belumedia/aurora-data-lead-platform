const axios = require('axios');
const qs = require('qs');
const Contacts = require('../models/Contacts');
const Logs = require('../models/Logs');
const LogsExports = require('../models/LogsExport');

const getAll = async (req, res, next) => {
  try {
    const { offset, limit } = req.query;
    const options = {
      offset,
      limit,
    };
    const contacts = await Contacts.paginate({}, options);
    res.status(200).json({ success: true, contacts });
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
};

const getByID = async (req, res, next) => {
  try {
  } catch (error) {}
};

const add = async (req, res, next) => {
  try {
    const params = req.body;
    const contact = await Contact.findOne({ email: params.email });

    if (contact) {
      console.log(`Contact with email: ${params.email} has been replaced`);
      Object.assign(contact, params);
      await contact.save();
    }
    // make a new const calling the List model

    if (!params.email) {
      throw 'Invalid Inputs';
    }

    const newContact = new Contacts(contactParam);

    await newContact.save();

    res.status(200).send({ contact: newContact });
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
};

const update = async (req, res) => {
  try {
    const { id } = req.params;
    const { items } = req.body;
    if (!id) {
      throw 'ID is missing';
    }
    const contact = await Contacts.findByIdAndUpdate(id, items);
    res.status(200).send({ contact, success: true });
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
};

const addBulk = async (req, res) => {
  try {
    const { items } = req.body;
    if (!items.length === 0) {
      throw 'Invalid Inputs';
    }
    const contacts = await Contacts.insertMany(items);
    res.status(200).send({ contacts: contacts, success: true });
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
};

const _import = async (req, res) => {
  try {
    res.status(200).send({ success: true });
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
};

const _export = async (req, res) => {
  try {
    const { contacts, list, url } = req.body;
    const { id } = req.user;

    if (contacts === undefined) {
      throw new Error('contacts empty');
    }

    const log = new Logs({
      user: id,
      operation: `Exporting Contacts to URL ${url}`,
      list,
    });

    await log.save();

    for await (const contact of contacts) {
      await axios
        .post(url, qs.stringify({ ...contact }))
        .then(async (response) => {
          const exported = response.data.includes(
            'Please correct the following errors'
          )
            ? false
            : true;
          const logExport = new LogsExports({
            contact: contact._id,
            log: log._id,
            exported,
          });
          await logExport.save();
        });
    }

    res
      .status(200)
      .send({ success: true, message: 'The List has been exported' });
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
};

// const _export = async (req, res) => {
//   try {
//     const { contacts, list, url } = req.body;
//     const { id } = req.user;

//     let axiosArray = [];

//     contacts.forEach((contact) => {
//       let newPromise = axios.post(url, null, { params: { ...contact } });
//       axiosArray.push(newPromise);
//     });

//     const requests = await axios.all(axiosArray);

//     const details = requests.map((request) => {
//       let requestStatus;
//       if (request.data.includes('Please correct the following errors:')) {
//         requestStatus = `Error while sending ${JSON.stringify(
//           request.config.params
//         )}`;
//       } else {
//         requestStatus = `Success while sending ${JSON.stringify(
//           request.config.params
//         )}`;
//       }
//       return requestStatus;

//     });

//     const logs = new Logs({
//       user: id,
//       operation: `Exporting Contacts to URL ${url}`,
//       list,
//       details,
//     });

//     await logs.save();

//     res
//       .status(200)
//       .send({ success: true, message: 'The List has been exported' });
//   } catch (error) {
//     res.status(400).send({ error: error.message });
//   }
// };

const _delete = async (req, res) => {
  try {
    const { id } = req.params;
    if (!id) {
      throw 'ID is missing';
    }
    const contact = await Contacts.findByIdAndDelete(id);
    res.status(200).send({ contact, success: true });
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
};

module.exports = {
  getAll,
  getByID,
  add,
  addBulk,
  update,
  _import,
  _export,
  _delete,
};
