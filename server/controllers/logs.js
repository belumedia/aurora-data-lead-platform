const Logs = require('../models/Logs');
const LogsExport = require('../models/LogsExport');
const Template = require('../models/Templates');

const getAll = async (req, res) => {
  try {
    const { offset, limit } = req.query;
    const options = {
      populate: ['user', 'list'],
      offset,
      limit,
      sort: { _id: -1 },
    };
    const logs = await Logs.paginate({}, options);
    res.status(200).json({ success: true, logs });
  } catch (error) {
    res.status(400).send({ success: false, error: error.message });
  }
};

const getByID = async (req, res) => {
  try {
    const { id } = req.params;
    const log = await Logs.findById(id).populate('list');
    if (!log) {
      throw new Error('Log id not found');
    }
    const template = await Template.findById(log.list.template, {
      name: false,
      _id: false,
      __v: false,
    });
    const logsExport = await LogsExport.find({ log: log._id })
      .lean()
      .populate('contact', '-_id -__v -validated -list');

    const contacts = logsExport.map((logExport) => ({
      ...logExport.contact,
      exported: logExport.exported,
    }));

    res.status(200).json({
      success: true,
      details: log,
      list: {
        headers: template.fields,
        contacts,
      },
    });
  } catch (error) {
    res.status(400).send({ success: false, error: error.message });
  }
};

module.exports = {
  getAll,
  getByID,
};
