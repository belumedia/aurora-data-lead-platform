const FieldsTemplate = require('../models/Templates');

const getAll = async (req, res) => {
    try {
        const templates = await FieldsTemplate.find({});
        res.status(200).send({ success: true, templates })
    } catch (error) {
        res.status(400).send({ error: error.message })
    }
}


const get = async (req, res) => {
    try {
        const { id } = req.params;
        const template = await FieldsTemplate.findById(id);
        res.status(200).send({ success: true, template })
    } catch (error) {
        res.status(400).send({ error: error.message })
    }
}


const add = async (req, res) => {
    try {
        const { name, fields } = req.body;
        if (!name || !fields) {
            throw new Error('Name or Fields are missing!')
        }
        const template = new FieldsTemplate({ name, fields });
        await template.save();
        res.status(200).send({ success: true, template })
    }
    catch (error) {
        res.status(400).send({ error: error.message })
    }
}

const update = async (req, res) => {
    try {
        const { id } = req.params;
        const { name, fields } = req.body;
        if (!id) {
            throw 'Template ID is missing';
        }
        const template = await FieldsTemplate.findByIdAndUpdate(id, { name, fields });
        res.status(200).send({ success: true, template })
    }
    catch (error) {
        res.status(400).send({ error: error.message })
    }
}

const _delete = async (req, res) => {
    try {
        const { id } = req.params;
        if (!id) {
            throw 'Template ID is missing';
        }
        const template = await FieldsTemplate.findByIdAndDelete(id);
        res.status(200).send({ success: true, template })
    } catch (error) {
        res.status(400).send({ error: error.message })
    }
}

module.exports = {
    getAll,
    get,
    add,
    update,
    _delete
}