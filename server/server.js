const express = require('express');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const app = express();
require('dotenv').config();
require('./db/mongoose');
const logger = require('morgan');
const http = require('http').Server(app);
const io = require('socket.io')(http);
const cors = require('cors');
const port = process.env.NODE_ENV === 'production' ? process.env.PORT : 3001;
const errorHandler = require('./helpers/error-handler');

// bodyParser, parses the request body to be a readable json format
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(cors());
app.use(fileUpload({ useTempFiles: true }));

//defining router
const routes = require('./routes/index');
app.use('/api/v1', routes);

if (process.env.NODE_ENV !== 'production') {
  app.use(logger('dev'));
}

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});

app.use(errorHandler);

app.listen(`${port}`, () => {
  console.log(`Server is listening on port: ${port}`);
});

io.on('connection', () => {
  console.log('a user is connected');
});
