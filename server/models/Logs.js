const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const logsSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    operation: {
      type: String,
      required: true,
    },
    list: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'List',
    },
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
  }
);

logsSchema.plugin(mongoosePaginate);
const Logs = mongoose.model('Logs', logsSchema);

module.exports = Logs;
