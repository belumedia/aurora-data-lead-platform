const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const Schema = mongoose.Schema;

const ContactsModelSchema = new Schema(
  {
    list: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'List',
    },
  },
  { strict: false }
);

ContactsModelSchema.plugin(mongoosePaginate);

const Contacts = mongoose.model('Contacts', ContactsModelSchema);

module.exports = Contacts;
