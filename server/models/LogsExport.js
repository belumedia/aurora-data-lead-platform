const mongoose = require('mongoose');

const logsExportSchema = mongoose.Schema(
  {
    contact: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Contacts',
    },
    log: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Logs',
    },
    exported: Boolean,
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
  }
);

const LogsExport = mongoose.model('LogsExport', logsExportSchema);

module.exports = LogsExport;
