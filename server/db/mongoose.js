const mongoose = require('mongoose');
const dbURL = process.env.MONGODB_URL;

mongoose
  .connect(dbURL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log('connected to MongoDB database');
  })
  .catch((err) => {
    console.log(err);
    console.log('failed connected to database');
  });
