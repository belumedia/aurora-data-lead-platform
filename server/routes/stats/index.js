const router = require('express').Router();
const { getAll } = require('../../controllers/stats');
const { admin } = require('../../middleware/auth');

router.get('/', getAll);

module.exports = router;
