const express = require('express');
const list = require('../../controllers/list');
const router = express.Router();
const { admin, auth } = require('../../middleware/auth');

router.route('/').get(auth, list.getAll);
router.route('/').post(admin, list.add);
router.route('/id/:id').get(auth, list.getById);
router.route('/export').post(admin, list._export);
router.route('/:id').delete(admin, list._delete);

module.exports = router;
