const router = require('express').Router();
const template = require('../../controllers/templates');
const { admin } = require('../../middleware/auth');

router.route('/').get(admin, template.getAll)
router.route('/:id').get(admin, template.get)
router.route('/').post(admin, template.add)
router.route('/:id').delete(admin, template._delete)
router.route('/:id').patch(admin, template.update)

module.exports = router;