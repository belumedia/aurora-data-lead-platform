const express = require('express');
const authController = require('../../controllers/auth');
const router = express.Router();
const { auth, checkToken } = require('../../middleware/auth');
const validator = require('../../helpers/validations.helper');

router.route('/login').post(validator.login, authController.login);
router.route('/logout').get(auth, authController.logout);
router.route('/logout-all').get(auth, authController.logoutAll);
router
  .route('/forgot-password')
  .post(validator.forgotPassword, authController.forgotPassword);
router.route('/check-code').post(validator.checkCode, authController.checkCode);
router
  .route('/reset-password')
  .post(validator.resetPassword, authController.resetPassword);
router.route('/refresh-token').post(checkToken, authController.refreshToken);

module.exports = router;
