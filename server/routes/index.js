const routes = require('express').Router();
const stats = require('./stats');
const authentication = require('./auth');
const users = require('./users');
const contacts = require('./contacts');
const lists = require('./lists');
const templates = require('./templates');
const files = require('./files');
const vendors = require('./vendors');
const logs = require('./logs');

routes.get('/', (req, res) => {
  res.status(200).json({ message: 'Connected!' });
});

routes.use('/stats', stats);
routes.use('/auth', authentication);
routes.use('/users', users);
routes.use('/contacts', contacts);
routes.use('/lists', lists);
routes.use('/templates', templates);
routes.use('/files', files);
routes.use('/vendors', vendors);
routes.use('/logs', logs);

module.exports = routes;
