import React from 'react';
import {
    Route,
    Redirect,
} from "react-router-dom";

const PrivateRoute = ({ component: Component, ...rest }) => {

    const authToken = localStorage.getItem('authToken');
    
    return (
        <Route {...rest} 
            render={props =>
                authToken ? ( <Component {...props} /> ) : ( <Redirect to={{ pathname: "/login", state: { from: props.location }}}  />)
            }
        />
    );
}

export default PrivateRoute;