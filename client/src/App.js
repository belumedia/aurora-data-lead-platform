import React from 'react';
import Routes from './Routes';
import { ToastProvider } from 'react-toast-notifications';


const App = () => {

  return (
    <ToastProvider>
      <Routes />
    </ToastProvider>
  );
};

export default App;