import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect } from "react-router-dom";
import PrivateRoute from './PrivateRoute';

import Login from './containers/auth/Login';
import ForgotPassword from './containers/auth/ForgotPassword';
import Dashboard from './containers/Dashboard';
import Lists from './containers/Lists';
import ListContacts from './containers/lists/ListContacts';
import Import from './containers/Import';
import Profile from './containers/Profile';
import Templates from './containers/Templates';
import AddTemplate from './containers/template/AddTemplate';
import Vendors from './containers/Vendors';
import Users from './containers/Users';
import AddUser from './containers/users/AddUser';
import Edituser from './containers/users/Edituser';
import AddVendors from './containers/vendors/AddVendors';
import EditVendors from './containers/vendors/EditVendors';
import EditTemplate from './containers/template/EditTemplate';
import Logs from './containers/Logs';

const Routes = ({ auth: { user } }) => {
  return (
    <Switch>
      <Route path="/login" component={Login} exact={true} />
      <Route path="/forgot-password" component={ForgotPassword} exact={true} />
      <PrivateRoute path="/" component={Dashboard} exact={true} />
      <PrivateRoute path="/lists" component={Lists} exact={true} />
      <PrivateRoute path="/lists/:id" component={ListContacts} exact={true} />
      <PrivateRoute path="/profile" component={Profile} exact={true} />
      {user.role === 'admin' ?
        <>
          <PrivateRoute path="/import" component={Import} exact={true} />
          <PrivateRoute path="/templates" component={Templates} exact={true} />
          <PrivateRoute path="/templates/add" component={AddTemplate} exact={true} />
          <PrivateRoute path="/templates/edit/:id" component={EditTemplate} exact={true} />
          <PrivateRoute path="/vendors" component={Vendors} exact={true} />
          <PrivateRoute path="/vendors/edit/:id" component={EditVendors} exact={true} />
          <PrivateRoute path="/vendors/add" component={AddVendors} exact={true} />
          <PrivateRoute path="/users" component={Users} exact={true} />
          <PrivateRoute path="/users/add" component={AddUser} exact={true} />
          <PrivateRoute path="/users/edit/:id" component={Edituser} exact={true} />
          <PrivateRoute path="/logs" component={Logs} exact={true} />
        </>
        :
        <Redirect to="/" />
      }

    </Switch>
  );
};

export default connect(state => ({ auth: state.auth }))(Routes);