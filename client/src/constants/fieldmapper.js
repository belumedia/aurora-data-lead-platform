export const fieldsMapperList = [
    { value: 'firstName', label: 'First Name' },
    { value: 'lastName', label: 'Last Name' },
    { value: 'email', label: 'Email' },
    { value: 'phone', label: 'Phone' },
    { value: 'jobTitle', label: 'Job Title' },
    { value: 'jobLevel', label: 'Job Level' },
    { value: 'jobType', label: 'Job Type' },
    { value: 'jobFunction', label: 'Job Function' },
    { value: 'direct', label: 'Direct' },
    { value: 'company', label: 'Company' },
    { value: 'address1', label: 'Address 1' },
    { value: 'address2', label: 'Address 2' },
    { value: 'city', label: 'City' },
    { value: 'state', label: 'State' },
    { value: 'postCode', label: 'Postcode' },
    { value: 'country', label: 'Country' },
    { value: 'industry', label: 'Industry' },
    { value: 'subMarket', label: 'Market' },
    { value: 'employees', label: 'Employees' },
    { value: 'webSite', label: 'Web Site' },
    { value: 'revenue', label: 'Revenue' },
    { value: 'validated', label: 'Validate' },
    { value: 'socialLink', label: 'Social Link' },
    { value: 'techIntent', label: 'Tech' }
];

export const fieldsFilterList = [
   
    { value: 'jobTitle', label: 'Job Title' },
    { value: 'jobLevel', label: 'Job Level' },
    { value: 'jobType', label: 'Job Type' },
    { value: 'jobFunction', label: 'Job Function' },
    { value: 'company', label: 'Company Name' },
    { value: 'city', label: 'City' },
    { value: 'state', label: 'State or Region' },
    { value: 'postCode', label: 'Postcode' },
    { value: 'country', label: 'Country' },
    { value: 'industry', label: 'Industry' },
    { value: 'employees', label: 'Employees' },
    { value: 'techIntent', label: 'Technology Install' },
];

// the field firstName, lastName, email, phone should be hidden 

export const fieldsTableList = [ 
    { value: 'firstName', label: 'First Name' },
    { value: 'lastName', label: 'Last Name' },
    { value: 'email', label: 'Email' },
    { value: 'phone', label: 'Phone' },
    { value: 'jobLevel', label: 'Level' },
    { value: 'jobTitle', label: 'Job Title' },
    { value: 'jobLevel', label: 'Level' },
    { value: 'jobFunction', label: 'Function' },
    { value: 'company', label: 'Company' },
    { value: 'employees', label: 'Employees' },
    { value: 'industry', label: 'Industry' },
    { value: 'city', label: 'City' },
    { value: 'state', label: 'State' },
    { value: 'country', label: 'Country' },
    { value: 'userConsent', label: 'User Consent' },
    { value: 'segment ', label: 'Segment' },
    { value: 'language ', label: 'Language' },
    { value: 'asset ', label: 'Asset' },
    { value: 'utmSource ', label: 'UTM Source' },
    { value: 'utmMedium', label: 'UTM Medium' },
    { value: 'utmCampaign', label: 'UTM Campaign' },
    { value: 'utmContent', label: 'UTM Content' }
];