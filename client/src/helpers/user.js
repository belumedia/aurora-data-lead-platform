export const greatings = () => {
  const today = new Date();
  const curHr = today.getHours();
  let greatings;
  if (curHr < 12) {
    greatings = 'Good morning';
  } else if (curHr < 18) {
    greatings = 'Good afternoon';
  } else {
    greatings = 'Good evening';
  }

  return greatings;
};
