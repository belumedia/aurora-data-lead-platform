import moment from 'moment';

export const parseCSV = (input) => {
    let [keys, ...rows] = input;
    return rows.map((row) => {
        return row.reduce((map, val, i) => {
            map[keys[i]] = val;
            return map;
        }, {});
    });
}

export const fortmatCSV = (rows, fieldsMapped) => {
    rows.shift();  // remove first array's item that is the table header from the CVS file
    // rows.pop(); // remove last row that excels is producing empty
    // map the rows and assign to a new array called map if the key is in fieldsMapped and return a new object 
    // with only the fields mapped 

    return rows.filter((row) => Array.isArray(row) && row.length > 1).map(row => {
        return row.reduce((map, val, key) => {
            if (key in fieldsMapped) {
                map[fieldsMapped[key]] = val;
            }
            return map;
        }, {});
    })

}

export const disabledDays = (start, end) => {
    const datesArr = [];
    let currentDate = start;
    let endDate = end;
    while (currentDate <= endDate) {
        datesArr.push(currentDate);
        currentDate = moment(currentDate).add(1, 'day')
    }
    return datesArr;
}