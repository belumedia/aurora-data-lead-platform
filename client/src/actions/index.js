

export const dispatchingRequest = (type) => {
    return {
        type
    }
}

export const successCall = (type, response) => {
    return {
        type: type,
        payload: response
    }
}

export const errorCall = (type, response) => {
    return {
        type: type,
        payload: response
    }
}