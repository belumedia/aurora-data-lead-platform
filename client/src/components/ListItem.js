import React, { useState } from 'react';
import styled from 'styled-components';
import { Motion, spring } from 'react-motion';
import { IoIosTrash, IoIosColorPalette } from 'react-icons/io';
import { Button } from './form/index';

const DeleteItem = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;

  display: flex;
  align-items: center;
  justify-content: center;
  background: rgba(255, 255, 255, 1);
  opacity: ${(props) => props.style.opacity};
  transform: translateY(${(props) => props.style.y}%);
`;

const ListItem = ({ id, name, description, image, onClick, onDelete }) => {
  const [isDeleting, setDeleting] = useState(false);

  const _onConfirm = () => {
    onDelete(id);
  };

  return (
    <div className="group bg-white shadow-sm rounded-md p-8 border border-gray-100 relative transition duration-150 ease-linear text-center cursor-pointer hover:shadow-lg">
      {!isDeleting && (
        <button className="absolute top-2 right-2 transform translate-x-8 -translate-y-8 shadow-sm border border-gray-600 rounded-full h-16 w-16 flex items-center justify-center bg-white text-red-500 opacity-0 transition-all duration-200 group-hover:opacity-100">
          <IoIosTrash onClick={() => setDeleting(!isDeleting)} />
        </button>
      )}
      <div
        className="h-full w-full overflow-hidden flex flex-col items-center"
        onClick={() => onClick(id)}
      >
        <div className="h-16 w-16 rounded-full flex items-center justify-center">
          {image ? (
            <img src={image} alt={name} className="w-full inline-block" />
          ) : (
            <IoIosColorPalette className="w-full inline-block h-12 text-gray-200" />
          )}
        </div>
        <h3 className="font-bold text-lg mt-4">{name}</h3>
        {description && (
          <span className="text-gray-400 text-center">{description}</span>
        )}
      </div>
      {isDeleting && (
        <Motion
          defaultStyle={{ y: 20, opacity: 0 }}
          style={{
            y: isDeleting ? spring(0) : spring(100),
            opacity: isDeleting ? spring(1) : spring(0),
          }}
        >
          {(interpolatedStyle) => (
            <DeleteItem
              style={{
                y: interpolatedStyle.y,
                opacity: interpolatedStyle.opacity,
              }}
            >
              <Button className="btn btn-primary" onClick={_onConfirm}>
                Confirm
              </Button>
              <Button
                className="btn btn-outlined"
                onClick={() => setDeleting(false)}
              >
                Cancel
              </Button>
            </DeleteItem>
          )}
        </Motion>
      )}
    </div>
  );
};

export default ListItem;
