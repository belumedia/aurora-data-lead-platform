import React from 'react';
import styled from 'styled-components';

const IconButtonItem = styled.button.attrs({ type: 'button' })`
  height: 2.5rem;
  width: 2.5rem;
  flex: 0 0 auto;
  color: rgba(0, 0, 0, 0.54);
  padding: 0.5rem;
  margin: 0 0.5rem;
  overflow: visible;
  font-size: 1.5rem;
  text-align: center;
  transition: background-color 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  border-radius: 50%;
  &:hover {
    background-color: var(--primary-gray);
  }
`;

export const IconButton = ({ children, onClick }) => {
  return <IconButtonItem onClick={onClick}>{children}</IconButtonItem>;
};
