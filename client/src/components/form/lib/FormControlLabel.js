import React from 'react';
import styled from 'styled-components';

const FormControlItem = styled.div`
  width: ${(props) => (props.fullWidth ? '100%' : 'auto')};
  display: ${(props) => (props.fullWidth ? 'block' : 'inline-block')};
`;

const Label = styled.label`
  cursor: pointer;
  display: inline-flex;
  align-items: center;
  margin-left: -11px;
  margin-right: 16px;
  vertical-align: middle;
  -webkit-tap-highlight-color: transparent;
`;

export const FormControlLabel = ({ control, label }) => {
  return (
    <FormControlItem className="form-control">
      <Label>
        {control}
        {label}
      </Label>
    </FormControlItem>
  );
};
