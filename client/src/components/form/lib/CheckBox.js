import React from 'react';

export const Checkbox = ({ ...props }) => {
    return (
        <div className="checkbox">
            <label htmlFor={props.id}>
                {props.label &&
                    <span className="label">{props.label}</span>
                }
                <input type="checkbox" {...props} />
                <span className="checkmark" />
            </label>
        </div>
    )
}