import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Motion, spring } from 'react-motion';
import { IoIosClose } from 'react-icons/io';

const DialogContainer = styled.div`
  height: 100vh;
  width: 100vw;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 15;
  background: var(--white);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  transform: translateX(${(props) => props.style.x}%);
`;

const Dialog = ({
  title,
  description,
  children,
  open,
  onCancel,
  loading,
  success,
  error,
}) => {
  return (
    <Motion
      defaultStyle={{ x: 100 }}
      style={{ x: open ? spring(0) : spring(100) }}
    >
      {(interpolatedStyle) => (
        <DialogContainer style={{ x: interpolatedStyle.x }}>
          <div className="absolute top-8 right-8 text-4xl cursor-pointer">
            <IoIosClose onClick={onCancel} />
          </div>
          <div className="max-w-md">
            <h2 className="font-bold mb-4 text-2xl">{title}</h2>
            <p className="mb-4 text-lg font-bold">{description}</p>
            {children}
          </div>
        </DialogContainer>
      )}
    </Motion>
  );
};

Dialog.propsType = {
  title: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  onSave: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  success: PropTypes.string.isRequired,
  error: PropTypes.string.isRequired,
};

Dialog.defaultProps = {
  title: 'Title',
};

export default Dialog;
