import React, { useState, useEffect, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Listbox } from '@headlessui/react';
import { levenshteinDistance } from '../../algorithms/string/levenshtein-distance/levenshteinDistance';

const FieldsMapperDropdown = ({
  item,
  index,
  handleChangeMapField,
  fieldsMapped,
  fieldsMapTemplateItem,
  fieldMapTemplate,
}) => {
  const [field, setField] = useState('');

  useEffect(() => {
    autoMapField();
  }, [fieldMapTemplate]);

  const autoMapField = () => {
    // Getting the closest word comparing a and b with Levenshtein distance algorithm

    fieldsMapTemplateItem.map(async ({ label, value }) => {
      const closestValue = levenshteinDistance(item, label);

      if (0 <= closestValue && closestValue <= 1) {
        setField(value);
        await handleChangeMapField({
          field: value,
          index,
        });
      }
    });
  };

  const handleChange = (value) => {
    const item = {
      field: value,
      index: index,
    };

    setField(value);
    handleChangeMapField(item);
  };

  const disabledOptions = (value) => {
    for (let k in fieldsMapped) {
      if (fieldsMapped[k] === value) {
        return true;
      }
    }
    return false;
  };

  return (
    <div className="mr-4 w-60 relative">
      <Listbox value={field} onChange={handleChange}>
        <Listbox.Button
          as="span"
          className="relative w-full bg-white border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-green-500 focus:border-green-500"
        >
          <span className="flex items-center">
            <span className="ml-3 block truncate">
              {field || 'Select a value'}
            </span>
          </span>
          <span className="ml-3 absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
            <svg
              className="h-5 w-5 text-gray-400"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fillRule="evenodd"
                d="M10 3a1 1 0 01.707.293l3 3a1 1 0 01-1.414 1.414L10 5.414 7.707 7.707a1 1 0 01-1.414-1.414l3-3A1 1 0 0110 3zm-3.707 9.293a1 1 0 011.414 0L10 14.586l2.293-2.293a1 1 0 011.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z"
                clipRule="evenodd"
              />
            </svg>
          </span>
        </Listbox.Button>
        <Listbox.Options className="absolute max-h-96 overflow-y-auto mt-1 w-full rounded-md bg-white shadow-lg outline-none">
          <Listbox.Option
            value=""
            className="cursor-default select-none relative py-2 pl-3 pr-9 hover:bg-gray-100"
          >
            Reset Vaue
          </Listbox.Option>
          {fieldsMapTemplateItem.map(({ label, value }) => (
            <Listbox.Option
              as={Fragment}
              key={value}
              value={value}
              disabled={disabledOptions(value)}
            >
              {({ selected, disabled }) => (
                <li
                  className={`cursor-default select-none relative py-2 pl-3 pr-9 hover:bg-gray-100 ${
                    disabled
                      ? 'bg-gray-100 text-gray-500 cursor-not-allowed'
                      : 'bg-white text-black cursor-pointer'
                  }`}
                >
                  {label}
                  {selected && (
                    <span className="absolute inset-y-0 right-0 flex items-center pr-4">
                      <svg
                        class="h-5 w-5"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                        aria-hidden="true"
                      >
                        <path
                          fill-rule="evenodd"
                          d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                          clip-rule="evenodd"
                        />
                      </svg>
                    </span>
                  )}
                </li>
              )}
            </Listbox.Option>
          ))}
        </Listbox.Options>
      </Listbox>
    </div>
  );
};

FieldsMapperDropdown.propTypes = {
  item: PropTypes.string,
  index: PropTypes.number,
};

export default FieldsMapperDropdown;
