import React from 'react';
import PropTypes from 'prop-types';
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io';
import FieldsMapperDropdown from './FieldsMapperDropdown';
import Pagination from 'react-paginate';

class List extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 0,
      rowsPerPage: 100,
      rows: [],
      rowsPerPageOptions: [],
    };
  }

  componentDidMount() {
    const { rows } = this.props;
    this.setState({
      rows,
    });
  }

  componentWillReceiveProps(nextProp) {
    if (nextProp.rows !== this.props.rows) {
      const { rows } = nextProp;
      this.setState({
        rows,
      });
    }
  }

  handleChangePage = ({ selected }) => {
    this.setState({
      page: selected,
    });
  };

  handleChangeRowsPerPage = (event) => {
    this.setState({
      rowsPerPage: parseInt(event.target.value),
    });
  };

  handleChangeMapField = (item) => {
    this.props.handleMapFields(item);
  };

  tableMapCell = (row, index) => {
    let rowCell = [];
    for (let value of row) {
      rowCell.push(
        <td className="px-6 py-4 whitespace-nowrap" key={index++}>
          {' '}
          {value}{' '}
        </td>
      );
    }
    return rowCell;
  };

  render() {
    const { rows, page, rowsPerPage } = this.state;
    const {
      fieldsMapped,
      fieldsMapTemplateItem,
      fieldMapTemplate,
    } = this.props;
    const [headers, ...items] = rows;

    if (this.state.rows.length > 0) {
      return (
        <div className="flex flex-col w-full">
          <div className="overflow-x-auto">
            <table className="min-w-full divide-y divide-gray-200 border-separate">
              <thead className="bg-gray-50">
                <tr>
                  {headers.map((item, index) => (
                    <th
                      key={index}
                      scope="col"
                      className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider whitespace-nowrap"
                    >
                      {item}
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                <tr>
                  {headers.map((item, index) => (
                    <td className="px-6 py-4 whitespace-nowrap" key={index}>
                      <FieldsMapperDropdown
                        fieldsMapTemplateItem={fieldsMapTemplateItem}
                        fieldsMapped={fieldsMapped}
                        fieldMapTemplate={fieldMapTemplate}
                        handleChangeMapField={this.handleChangeMapField}
                        item={item}
                        index={index}
                      />
                    </td>
                  ))}
                </tr>
                {items
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => (
                    <tr key={index}>{this.tableMapCell(row, index)}</tr>
                  ))}
              </tbody>
            </table>
          </div>
          {items.length > rowsPerPage && (
            <Pagination
              containerClassName="pagination"
              previousLabel={<IoIosArrowBack />}
              nextLabel={<IoIosArrowForward />}
              initialPage={page}
              onPageChange={this.handleChangePage}
              pageRangeDisplayed={5}
              marginPagesDisplayed={2}
              pageCount={items.length / rowsPerPage}
            />
          )}
        </div>
      );
    } else {
      return (
        <h3 className="empty-list font-bold">No contacts imported yet...</h3>
      );
    }
  }
}

List.propTypes = {
  rows: PropTypes.array.isRequired,
};

export default List;
