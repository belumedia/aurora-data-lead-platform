import React from 'react';
import { Button } from '../form';
import { IoIosEye } from 'react-icons/io';

const ListItem = ({
  _id,
  user,
  list,
  operation,
  created_at,
  handleFetchLog,
}) => {
  return (
    <>
      <tr>
        <td className="px-6 py-4 whitespace-nowrap">{user?.email}</td>
        <td className="px-6 py-4 whitespace-nowrap">{operation}</td>
        <td className="px-6 py-4 whitespace-nowrap">{list?.name}</td>
        <td className="px-6 py-4 whitespace-nowrap">{created_at}</td>
        <td className="px-6 py-4 whitespace-nowrap">
          <Button onClick={() => handleFetchLog(_id)}>
            <IoIosEye />
          </Button>
        </td>
      </tr>
    </>
  );
};

export default ListItem;
