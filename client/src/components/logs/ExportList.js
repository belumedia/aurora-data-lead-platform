import React from 'react';
import PropTypes from 'prop-types';
import { IoIosAlert, IoIosCheckmark } from 'react-icons/io';

function List({ rows, headers }) {
  if (rows.length > 0) {
    return (
      <div className="flex flex-col w-full">
        <div className="overflow-y-auto">
          <table className="min-w-full divide-y divide-gray-200">
            <thead className="bg-gray-50">
              <tr>
                <th
                  scope="col"
                  className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider whitespace-nowrap"
                >
                  Exported
                </th>
                {headers.map(({ label }, index) => (
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider whitespace-nowrap"
                    key={index}
                    data-index={index}
                  >
                    {label}
                  </th>
                ))}
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-200">
              {rows.map((item, index) => (
                <tr key={index}>
                  <td className="px-6 py-4 whitespace-nowrap">
                    {item.exported ? (
                      <span className="text-green-500">
                        <IoIosCheckmark />
                      </span>
                    ) : (
                      <span className="text-red-500">
                        <IoIosAlert />
                      </span>
                    )}
                  </td>
                  {headers.map(({ value }, index) => (
                    <td key={index} className="px-6 py-4 whitespace-nowrap">
                      {item[value]}
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  } else {
    return <h3 className="empty-list font-bold"> No Items found</h3>;
  }
}

List.propTypes = {
  rows: PropTypes.array.isRequired,
};

export default List;
