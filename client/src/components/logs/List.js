import React from 'react';
import PropTypes from 'prop-types';
import ListItem from './ListItem';

function List({ rows, handleFetchLog }) {
  if (rows.length > 0) {
    return (
      <div className="flex flex-col w-full">
        <div className="overflow-y-auto">
          <table className="min-w-full divide-y divide-gray-200">
            <thead className="bg-gray-50">
              <tr>
                <th
                  scope="col"
                  className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                >
                  User
                </th>
                <th
                  scope="col"
                  className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                >
                  Operation
                </th>
                <th
                  scope="col"
                  className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                >
                  List
                </th>
                <th
                  scope="col"
                  className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                >
                  Created At
                </th>
                <th />
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-200">
              {rows.map(
                ({ _id, user, operation, details, list, created_at }) => (
                  <ListItem
                    _id={_id}
                    user={user}
                    list={list}
                    operation={operation}
                    details={details}
                    created_at={created_at}
                    key={_id}
                    handleFetchLog={handleFetchLog}
                  />
                )
              )}
            </tbody>
          </table>
        </div>
      </div>
    );
  } else {
    return <h3 className="empty-list font-bold">No logs found</h3>;
  }
}

List.propTypes = {
  rows: PropTypes.array.isRequired,
};

export default List;
