import React, { Component } from 'react';
import { Field, FieldArray, reduxForm } from 'redux-form';
import { FormControl, Button, IconButton, Divider, Checkbox } from '../form';
import { IoIosTrash, IoIosAddCircle } from 'react-icons/io';
import styled, { css } from 'styled-components';

const Grid = styled.div`
    ${props => props.container && css`
        display:grid;
        grid-template-columns: auto auto auto auto;
        grid-gap: ${props => props.spacing}rem;
   `}
    ${props => props.item && css`
        display: flex;
        align-items: ${props.alignItems ? props.alignItems : 'unset'}
    `}
`;


const validate = values => {
    const errors = {}
    if (!values.templateName) {
        errors.templateName = 'Required'
    }
    if (!values.templateFields || !values.templateFields.length) {
        errors.templateFields = { _error: 'At least one Template Item must be entered' }
    } else {
        const templateArrayErrors = []
        values.templateFields.forEach((templateField, templateFieldIndex) => {
            const templateErrors = {}
            if (!templateField || !templateField.label) {
                templateErrors.label = 'Required'
                templateArrayErrors[templateFieldIndex] = templateErrors
            }
            if (!templateField || !templateField.value) {
                templateErrors.value = 'Required'
                templateArrayErrors[templateFieldIndex] = templateErrors
            }
        })
        if (templateArrayErrors.length) {
            errors.templateFields = templateArrayErrors
        }
    }
    return errors
}

const renderField = (field) => {

    return (
        <FormControl fullWidth error={field.meta.touched && field.meta.error}>
            {field.label && <label htmlFor={field.id}>{field.label}</label>}
            <input {...field.input} type={field.type} />
            {field.meta.touched && field.meta.error &&
                <span className="error">{field.meta.error}</span>
            }
        </FormControl>
    )
}


const renderCheckBox = (field) => (
   
    <FormControl fullWidth error={field.meta.touched && field.meta.error}>
        <Checkbox label={field.label} {...field.input} />
        {field.meta.touched && field.meta.error &&
            <span className="error">{field.meta.error}</span>
        }
    </FormControl>
)

const renderTemplateField = ({ fields, meta: { touched, error } }) => {


    return (
        <>

            {fields.length === 0 &&
                <FormControl>
                    <Button onClick={() => fields.push({})}>
                        <IoIosAddCircle /> Start adding a Template Field
                    </Button>
                </FormControl>
            }

            <FormControl>
                {touched && error && <span>{error}</span>}
            </FormControl>

            {fields.map((field, index) =>
                <FormControl fullWidth key={index}>
                    <Grid container spacing={2}>
                        <Grid item>

                            <Field
                                name={`${field}.label`}
                                id={`${field}.label`}
                                type="text"
                                component={renderField}
                                label="Label"
                            />

                        </Grid>
                        <Grid item>

                            <Field
                                name={`${field}.value`}
                                id={`${field}.value`}
                                type="text"
                                component={renderField}
                                label="Value"
                            />

                        </Grid>
                        <Grid item alignItems="flex-end">

                            <Field
                                name={`${field}.filterable`}
                                id={`${field}.filterable`}
                                type="checkbox"
                                component={renderCheckBox}
                                label="Filterable"
                            />

                        </Grid>
                        <Grid item alignItems="flex-end">

                            <IconButton onClick={() => fields.push({})}>
                                <IoIosAddCircle />
                            </IconButton>
                            <IconButton onClick={() => fields.remove(index)}>
                                <IoIosTrash />
                            </IconButton>

                        </Grid>
                    </Grid>
                </FormControl>
            )}

        </>
    )
}


class TemplateForm extends Component {

    render() {

        const { handleSubmit, pristine, reset, submitting } = this.props;

        return (
            <form onSubmit={handleSubmit}>

                <Field
                    id="templateName"
                    label="Template Name"
                    name="templateName"
                    type="text"
                    component={renderField}
                />

                <FieldArray name="templateFields" component={renderTemplateField} />

                <Divider />

                <FormControl fullWidth>
                    <Button variant="outlined" color='primary' onClick={reset} disabled={pristine || submitting}>
                        Reset
                    </Button>
                    <Button type="submit" variant="contained" color='primary' disabled={submitting}>
                        Save Template
                    </Button>
                </FormControl>

            </form>
        )
    }

}


export default reduxForm({ form: 'TemplateForm', validate, enableReinitialize: true  })(TemplateForm)