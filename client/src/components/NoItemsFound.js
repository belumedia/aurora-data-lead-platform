import React from 'react';
import styled from 'styled-components';

import { IoIosSad } from 'react-icons/io';

const NoItemsFoundItem = styled.div`
    background-color: var(--white);
    padding: 2rem;
    margin: 2rem;
    border-radius: var(--border-radius-large)
`;

const NoItemsFoundHeader = styled.div`
    display:grid;
    grid-template-columns: 100px 1fr;
    grid-gap: 2rem;
`;

const NoItemsFoundIcon = styled.div`
    height: 100px;
    width: 100px;
    background-color: var(--primary-green);
    padding:1rem;
    border-radius: 100%;
    display: flex;
    align-items:center;
    justify-content: center;
    font-size:2rem;
    color: var(--white);
`;

const NoItemsFoundText = styled.div`
    display: flex;
    flex-direction: column;
`;

const NoItemsFoundTitle = styled.h3`
    margin-bottom: 0;
`;

const NoItemsFoundDescription = styled.p`
    color: var(--gray);
`;


const NoItemsFound = ({title, text}) => {
    return (
        <NoItemsFoundItem>
            <NoItemsFoundHeader>
                
                <NoItemsFoundIcon>
                    <IoIosSad/>
                </NoItemsFoundIcon>
                <NoItemsFoundText>
                    <NoItemsFoundTitle>
                        {title}
                    </NoItemsFoundTitle>
                    <NoItemsFoundDescription>
                        {text}
                    </NoItemsFoundDescription>
                </NoItemsFoundText>
            </NoItemsFoundHeader>
        </NoItemsFoundItem>
    );
};

export default NoItemsFound;