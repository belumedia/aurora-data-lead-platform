import React from 'react';

const ContentWrapper = ({ children }) => {
  return <div className="w-full h-full">{children}</div>;
};

export default ContentWrapper;
