import React from 'react';
import logoImg from '../assets/img/aurora.png'

const Logo = () => <img src={logoImg} />
       
export default Logo;