import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { Button, FormControl } from '../form';

const validate = (values) => {
  const errors = {};
  const requiredFields = ['password', 'confirmPassword'];
  requiredFields.forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Required';
    }
  });
  if (values.password !== values.confirmPassword) {
    errors.confirmPassword =
      'The password is not matching with the Password above';
  }
  return errors;
};

const renderTextField = ({
  label,
  input,
  meta: { touched, invalid, error },
  ...custom
}) => (
  <FormControl fullWidth error={touched && error}>
    {label && <label htmlFor={custom.id}>{label}</label>}
    <input {...input} {...custom} type={custom.type} />
    {touched && error && <span className="error">{error}</span>}
  </FormControl>
);

const ResetPasswordForm = (props) => {
  const { handleSubmit, pristine, submitting } = props;

  return (
    <form onSubmit={handleSubmit}>
      <div className="mb-4">
        <label className="font-medium mb-2 inline-block">Password</label>
        <Field
          variant="outlined"
          required
          type="password"
          id="password"
          name="password"
          autoComplete="password"
          component={renderTextField}
        />
      </div>
      <div className="mb-4">
        <label className="font-medium mb-2 inline-block">
          Confirm Password
        </label>
        <Field
          variant="outlined"
          required
          type="password"
          id="confirmPassword"
          name="confirmPassword"
          autoComplete="confirmPassword"
          component={renderTextField}
        />
      </div>
      <div className="mb-4">
        <Button
          type="submit"
          className="group relative w-full flex justify-center p-4 border border-transparent text-xm font-medium rounded-md text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
          disabled={pristine || submitting}
        >
          Reset Password
        </Button>
      </div>
      <div className="mb-4">
        <Link to="/login">Have you the login data?</Link>
      </div>
    </form>
  );
};

export default reduxForm({ form: 'ResetPasswordForm', validate })(
  ResetPasswordForm
);
