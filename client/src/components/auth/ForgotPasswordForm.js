import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { Button, FormControl } from '../form';

const validate = (values) => {
  const errors = {};
  const requiredFields = ['email', 'password'];
  requiredFields.forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Required';
    }
  });
  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
  ) {
    errors.email = 'Invalid email address';
  }
  return errors;
};

const renderTextField = ({
  label,
  input,
  meta: { touched, invalid, error },
  ...custom
}) => (
  <FormControl fullWidth error={touched && error}>
    {label && <label htmlFor={custom.id}>{label}</label>}
    <input {...input} {...custom} type={custom.type} />
    {touched && error && <span className="error">{error}</span>}
  </FormControl>
);

const ForgotPasswordForm = ({ handleSubmit, pristine, submitting }) => {
  return (
    <form onSubmit={handleSubmit}>
      <div className="mb-4">
        <label className="font-medium mb-2 inline-block">Email Address</label>
        <Field
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="email"
          type="email"
          name="email"
          autoComplete="email"
          component={renderTextField}
        />
      </div>
      <div className="mb-4">
        <Button
          type="submit"
          className="group relative w-full flex justify-center p-4 border border-transparent text-xm font-medium rounded-md text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
          disabled={pristine || submitting}
        >
          Reset Password
        </Button>
      </div>

      <div className="mb-4">
        <Link to="/login" variant="body2">
          Have you the login data?
        </Link>
      </div>
    </form>
  );
};

export default reduxForm({ form: 'ForgotPasswordForm', validate })(
  ForgotPasswordForm
);
