import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { FormControl, Button } from '../form';


const validate = values => {
    const errors = {}
    const requiredFields = [
        'firstname',
        'lastname',
        'phone'
    ]
    
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
   
    if (
        values.oldPassword && values.oldPassword.length < 8
    ) {
        errors.oldPassword = 'Password is too short'
    }

    if (
        values.password && values.password.length < 8
    ) {
        errors.password = 'Password is too short'
    }

    if (
        values.password !== values.confirmPassword
    ) {
        errors.confirmPassword = 'Password and Confirm Password are not matching'
    }

    return errors
}


const renderField = (field) => (
    <FormControl {...field} error={field.meta.touched && field.meta.error}>
        {field.label && <label>{field.label}</label>}
        <input {...field.input} type={field.type} />
        {field.meta.touched && field.meta.error &&
            <span className="error">{field.meta.error}</span>
        }
    </FormControl>
)


const ProfileForm = ({ handleSubmit, pristine, submitting, invalid, profileInfo }) => {


    return (

        <form onSubmit={handleSubmit}>

    
            <Field
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="firstname"
                label="First Name"
                type="text"
                id="firstname"
                autoComplete="firstname"
                component={renderField}
            />

            <Field
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="lastname"
                label="Last Name"
                type="text"
                id="lastname"
                autoComplete="lastname"
                component={renderField}
            />

            <Field
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="phone"
                label="Phone"
                type="text"
                id="phone"
                autoComplete="phone"
                component={renderField}
            />

            <FormControl fullWidth>
                <Button type="submit" disabled={pristine || submitting || invalid}>
                    Update
                </Button>
            </FormControl>
        </form>
 
    )
}

export default reduxForm({ form: 'ProfileForm', validate })(ProfileForm)