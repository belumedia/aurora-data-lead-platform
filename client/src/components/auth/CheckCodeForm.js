import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { Button, FormControl } from '../form';

const validate = (values) => {
  const errors = {};
  const requiredFields = ['code'];
  requiredFields.forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Required';
    }
  });
  if (values.code && /^.{5}$/.test(values.code)) {
    errors.email = 'Invalid code length';
  }
  return errors;
};

const renderTextField = ({
  label,
  input,
  meta: { touched, invalid, error },
  ...custom
}) => (
  <FormControl fullWidth error={touched && error}>
    {label && <label htmlFor={custom.id}>{label}</label>}
    <input {...input} {...custom} type={custom.type} />
    {touched && error && <span className="error">{error}</span>}
  </FormControl>
);

const CheckCodeForm = (props) => {
  const { handleSubmit, pristine, submitting } = props;

  return (
    <form onSubmit={handleSubmit}>
      <div className="mb-4">
        <label className="font-medium mb-2 inline-block">Code</label>
        <Field
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="code"
          name="code"
          type="text"
          component={renderTextField}
        />
      </div>
      <div className="mb-4">
        <Button
          type="submit"
          className="group relative w-full flex justify-center p-4 border border-transparent text-xm font-medium rounded-md text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
          disabled={pristine || submitting}
        >
          Request New Password
        </Button>
      </div>
      <div className="mb-4">
        <Link to="/forgot-password">Have you not received the code yet?</Link>
      </div>
    </form>
  );
};

export default reduxForm({ form: 'CheckCodeForm', validate })(CheckCodeForm);
