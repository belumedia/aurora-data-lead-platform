import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { Checkbox, Button, FormControl } from '../form';

const validate = (values) => {
  const errors = {};
  const requiredFields = ['email', 'password'];
  requiredFields.forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Required';
    }
  });
  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
  ) {
    errors.email = 'Invalid email address';
  }
  return errors;
};

const renderTextField = ({
  label,
  input,
  meta: { touched, invalid, error },
  ...custom
}) => (
  <FormControl fullWidth error={touched && error}>
    {label && <label htmlFor={custom.id}>{label}</label>}
    <input {...input} {...custom} type={custom.type} />
    {touched && error && <span className="error">{error}</span>}
  </FormControl>
);

const renderCheckbox = ({ input, label }) => (
  <Checkbox
    label={label}
    checked={input.value ? true : false}
    onChange={input.onChange}
  />
);

const LoginForm = (props) => {
  const { handleSubmit, pristine, submitting } = props;

  return (
    <form onSubmit={handleSubmit}>
      <div className="mb-4">
        <label className="font-medium mb-2 inline-block">Email Address</label>
        <Field
          variant="outlined"
          margin="normal"
          required
          id="email"
          name="email"
          type="email"
          autoComplete="email"
          component={renderTextField}
        />
      </div>
      <div className="mb-4">
        <div className="flex items-center justify-between mb-2">
          <label className="font-medium inline-block">Password</label>
          <Link to="/forgot-password" className="font-medium">
            Forgot password?
          </Link>
        </div>

        <Field
          variant="outlined"
          margin="normal"
          required
          name="password"
          type="password"
          id="password"
          autoComplete="current-password"
          component={renderTextField}
        />
      </div>
      <div className="mb-4">
        <Field name="remember" component={renderCheckbox} label="Remember me" />
      </div>
      <div className="mb-4">
        <Button
          className="group relative w-full flex justify-center p-4 border border-transparent text-xm font-medium rounded-md text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
          type="submit"
          disabled={pristine || submitting}
        >
          <>
            {submitting && (
              <>
                <svg
                  className="animate-spin -ml-1 mr-3 h-5 w-5 text-white"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                >
                  <circle
                    className="opacity-25"
                    cx="12"
                    cy="12"
                    r="10"
                    stroke="currentColor"
                    stroke-width="4"
                  ></circle>
                  <path
                    class="opacity-75"
                    fill="currentColor"
                    d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                  ></path>
                </svg>
                <span>Processing</span>
              </>
            )}
            {!submitting && <span>Sign In</span>}
          </>
        </Button>
      </div>
      <div></div>
    </form>
  );
};

export default reduxForm({ form: 'LoginForm', validate })(LoginForm);
