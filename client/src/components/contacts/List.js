import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ItemList from './list/ItemList';

const ListWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin-bottom: 75px;
`;

class List extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      rows: [],
      headers: [],
    };
  }

  componentDidMount() {
    const { rows, headers } = this.props;
    this.setState({
      rows,
      headers,
    });
  }

  componentWillReceiveProps(nextProp) {
    if (nextProp.rows !== this.props.rows) {
      const { rows, headers } = nextProp;
      this.setState({
        rows,
        headers,
      });
    }
  }

  shouldComponentUpdate(nextProps) {
    return (
      nextProps.rows !== this.props.rows ||
      nextProps.selected !== this.props.selected
    );
  }

  handleChangeRowsPerPage = (event) => {
    this.setState({
      rowsPerPage: parseInt(event.target.value),
    });
  };

  isSelected = (id) => this.props.selected.indexOf(id) !== -1;

  render() {
    const { headers, rows } = this.state;
    const { handleSelectItem, handleDeleteItem } = this.props;

    if (rows.length > 0) {
      return (
        <ListWrapper>
          <div className="overflow-x-auto">
            <table className="min-w-full divide-y divide-gray-200">
              <thead className="bg-gray-50">
                <tr>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                  />
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                  />
                  {headers.map(({ label }, index) => (
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider whitespace-nowrap"
                      key={index}
                      data-index={index}
                    >
                      {label}
                    </th>
                  ))}
                </tr>
              </thead>

              <tbody className="bg-white divide-y divide-gray-200">
                {rows.map((item, index) => (
                  <ItemList
                    item={item}
                    headers={headers}
                    isSelected={this.isSelected(item._id)}
                    handleSelectItem={handleSelectItem}
                    handleDeleteItem={handleDeleteItem}
                    key={`${item.id}-${index}`}
                  />
                ))}
              </tbody>
            </table>
          </div>
        </ListWrapper>
      );
    } else {
      return <h3 className="font-bold text-2xl">No contacts found</h3>;
    }
  }
}

List.propTypes = {
  rows: PropTypes.array.isRequired,
};

export default List;
