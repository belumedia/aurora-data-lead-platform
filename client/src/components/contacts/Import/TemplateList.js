import React, { Fragment } from 'react';
import { Listbox } from '@headlessui/react';
import { IoIosCheckmark } from 'react-icons/io';

const TemplateList = ({ list, fieldMapTemplate, handleChangeTemplate }) => {
  return (
    <div className="mr-4 w-60 relative">
      <Listbox value={fieldMapTemplate} onChange={handleChangeTemplate}>
        <Listbox.Button
          as="span"
          className="relative w-full bg-white border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-green-500 focus:border-green-500"
        >
          <span className="flex items-center">
            <span className="ml-3 block truncate">
              {list.find((item) => item._id === fieldMapTemplate)?.name ??
                'Select a template'}
            </span>
          </span>
          <span className="ml-3 absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
            <svg
              className="h-5 w-5 text-gray-400"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fillRule="evenodd"
                d="M10 3a1 1 0 01.707.293l3 3a1 1 0 01-1.414 1.414L10 5.414 7.707 7.707a1 1 0 01-1.414-1.414l3-3A1 1 0 0110 3zm-3.707 9.293a1 1 0 011.414 0L10 14.586l2.293-2.293a1 1 0 011.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z"
                clipRule="evenodd"
              />
            </svg>
          </span>
        </Listbox.Button>
        <Listbox.Options className="absolute mt-1 w-full rounded-md bg-white shadow-lg outline-none">
          {list.map((template) => (
            <Listbox.Option
              as={Fragment}
              key={template._id}
              value={template._id}
            >
              {({ active, selected }) => (
                <li
                  className={`text-gray-900 cursor-default select-none relative py-2 pl-3 pr-9 ${
                    active ? 'bg-gray-100 text-gray-900' : 'bg-white text-black'
                  }`}
                >
                  {template.name}
                  {selected && (
                    <span class="absolute inset-y-0 right-0 flex items-center pr-4">
                      <svg
                        class="h-5 w-5"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                        aria-hidden="true"
                      >
                        <path
                          fill-rule="evenodd"
                          d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                          clip-rule="evenodd"
                        />
                      </svg>
                    </span>
                  )}
                </li>
              )}
            </Listbox.Option>
          ))}
        </Listbox.Options>
      </Listbox>
    </div>
  );
};

export default TemplateList;
