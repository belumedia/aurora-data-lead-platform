import React, { useState, useEffect, memo } from 'react';
import styled from 'styled-components';
import { Checkbox } from '../../form';
import { IoIosCheckmark, IoIosAlert } from 'react-icons/io';

const ItemList = ({ item, headers, isSelected, handleSelectItem }) => {
  const [selected, setSelected] = useState(false);

  useEffect(() => {
    setSelected(isSelected);
  }, [isSelected]);

  return (
    <tr isSelected={selected}>
      <td className="px-6 py-4 whitespace-nowrap">
        {item.validated &&
        item.validated.email &&
        item.validated.email.isValid ? (
          <span className="text-green-500">
            <IoIosCheckmark />
          </span>
        ) : (
          <span className="text-red-500">
            <IoIosAlert />
          </span>
        )}
      </td>
      <td className="px-6 py-4 whitespace-nowrap">
        <Checkbox
          checked={selected}
          onChange={() => handleSelectItem(item._id)}
        />
      </td>
      {headers.map(({ value }, index) => (
        <td key={index} className="px-6 py-4 whitespace-nowrap">
          {item[value]}
        </td>
      ))}
    </tr>
  );
};

export default memo(ItemList);
