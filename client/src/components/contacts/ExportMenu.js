import React, { useEffect, useState } from 'react';
import { CSVLink } from 'react-csv';
import ExportForm from './Export/ExportForm';
import Side from '../Side';
import { IoIosDownload } from 'react-icons/io';

const ExportMenu = ({
  open,
  list,
  data,
  selected,
  handleExport,
  handleExportContactsToURL,
}) => {
  const [items, setItems] = useState(data);
  const [contactsFiltered, setContactsFiltered] = useState(false);

  useEffect(() => {
    filterSelectedContacts(data);
  }, [data, selected]);

  const filterSelectedContacts = (items) => {
    const hashItem = {};

    for (let item of selected) {
      hashItem[item] = item;
    }

    const filtered = items.reduce((contacts, contact) => {
      if (contact._id === hashItem[contact._id]) {
        contacts.push(contact);
      }
      return contacts;
    }, []);

    if (filtered.length > 0) {
      setContactsFiltered(true);
      setItems(filtered);
    } else {
      setContactsFiltered(false);
      setItems(items);
    }
  };

  const exportToURL = ({ url }) => {
    handleExportContactsToURL(items, list?._id, url);
  };

  return (
    <Side
      anchor="left"
      open={open}
      handleToggle={handleExport}
      closeText="Close Filters"
    >
      <div className="side--content">
        <h4 className="font-bold mb-4">Dispatch the List to an url</h4>
        <ExportForm onSubmit={exportToURL} />
        <h4 className="font-bold mb-4">Download CSV File</h4>
        {items.length > 0 && (
          <ul>
            <li>
              <IoIosDownload />
              <CSVLink filename={list?.name} data={items}>
                {contactsFiltered ? `Filtered-${list.name}` : list?.name}{' '}
              </CSVLink>
            </li>
          </ul>
        )}
      </div>
    </Side>
  );
};

export default ExportMenu;
