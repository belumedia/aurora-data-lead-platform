import React, { useState } from 'react';
import moment from 'moment';
import "react-dates/initialize";
import { DateRangePicker, isInclusivelyAfterDay } from 'react-dates';
import { START_DATE, END_DATE } from "react-dates/constants";
import "react-dates/lib/css/_datepicker.css";

const SearchByDates = ({ onChange }) => {

    const [rangeDate, setRangeDate] = useState({
        startDate: null,
        endDate: null
    })
    
    const [focused, setFocused] = useState(null);

    const onFocusChange = focused => setFocused(focused);

    const onDatesChange = ({ startDate, endDate }) => {
        setRangeDate({ startDate, endDate })
        if (focused === 'endDate') {
            if (startDate && endDate) {
                const start = moment(startDate).startOf("day");
                const end = moment(endDate).endOf("day");
                onChange(start, end)
            }
        };
    }

    return (
        <DateRangePicker
            startDate={rangeDate.startDate}
            startDateId={START_DATE}
            endDate={rangeDate.endDate}
            endDateId={END_DATE}
            onDatesChange={onDatesChange}
            focusedInput={focused}
            onFocusChange={onFocusChange}
            maxDate={moment()}
            numberOfMonths={window.innerWidth < 600 ? 1 : 2}
            initialVisibleMonth={() => moment().subtract(1, "M")}
            isOutsideRange={() => false}
            isOutsideRange={day => isInclusivelyAfterDay(day, moment().add(1, "day"))}
        />
    );
};

export default SearchByDates;