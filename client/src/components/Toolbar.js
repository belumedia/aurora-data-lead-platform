import React from 'react';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import styled from 'styled-components';
import { IoIosArrowBack } from 'react-icons/io';
import { unAuthenticate } from '../services/authentication.service';
import Avatar from './user/Avatar';
import UserInfo from './user/UserInfo';
import DropDownMenu from './DropDownMenu';
import VendorInfo from './user/VendorInfo';
import { Button } from './form';

const ToolbarContainer = styled.div`
  position: fixed;
  top: 0;
  z-index: 1;
  left: 75px;
  height: 75px;
  width: calc(100vw - 4rem);
  padding: 0.5rem 2rem;
  display: flex;
  background: white;
  border-bottom: 1px solid var(--primary-gray);
  box-shadow: rgba(67, 117, 151, 0.08) 0px 3px 7px 0px;
`;

const ToolbarItem = styled.div`
  &:nth-child(1) {
    display: flex;
    align-items: center;
    justify-content: flex-start;
  }
  &:nth-child(2) {
    margin-left: auto;
    justify-self: flex-end;
    display: flex;
    align-items: center;
    justify-content: flex-end;
  }
  &:nth-child(3) {
    margin-left: 1rem;
    justify-self: flex-end;
    display: flex;
    align-items: center;
    justify-content: flex-end;
  }
`;

const Title = styled.h4`
  margin: 0;
`;

const Toolbar = ({
  title,
  children,
  auth,
  history,
  goBack,
  unAuthenticate,
}) => {
  const getInitials = (letters) => {
    return letters?.split('')[0];
  };

  const onUnAuthenticate = () => {
    unAuthenticate().then(() => {
      localStorage.clear();
      history.push('/');
    });
  };

  return (
    <ToolbarContainer>
      <ToolbarItem>
        {goBack && (
          <Button className="btn btn-outlined btn-back" onClick={goBack}>
            <IoIosArrowBack /> Go Back
          </Button>
        )}
        <Title className="font-bold text-xl">{title}</Title>
      </ToolbarItem>
      <ToolbarItem>{children}</ToolbarItem>
      <ToolbarItem>
        <VendorInfo />
        <UserInfo info={auth.user} />
        <DropDownMenu
          title="Menu"
          items={[
            { label: 'Profile', link: '/profile' },
            { label: 'Logout', action: () => onUnAuthenticate() },
          ]}
        >
          <Avatar
            initial={getInitials(auth.user.username || auth.user.email)}
          />
        </DropDownMenu>
      </ToolbarItem>
    </ToolbarContainer>
  );
};

Toolbar.defaultProps = {
  title: '...',
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ unAuthenticate }, dispatch);
};

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(Toolbar);
