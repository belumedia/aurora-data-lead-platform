import React from 'react';
import { CSVLink } from 'react-csv';
import { IoIosArrowDown, IoIosDownload } from 'react-icons/io';
import { Button } from './form';

function ExportLists({ items, itemsLists, handleExportLists }) {
  return (
    <div className="flex items-center">
      {itemsLists.length > 0 && (
        <CSVLink
          filename=""
          data={itemsLists}
          className="inline-flex items-center p-2 no-underline"
        >
          Download generated CSV
          <IoIosDownload className="ml-2" />
        </CSVLink>
      )}

      {items.length > 0 && (
        <span
          class="group mx-2 px-4 py-2 rounded text-red-300 cursor-pointer hover:text-green-500"
          onClick={handleExportLists}
        >
          Export selected list
          <strong class="bg-red-100 group-hover:bg-green-800 group-hover:text-white h-8 inline-flex justify-center ml-1 p-1 rounded-full w-8">
            {items.length}
          </strong>
        </span>
      )}
    </div>
  );
}

export default ExportLists;
