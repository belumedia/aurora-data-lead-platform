import React, { useState } from 'react';
import styled from 'styled-components';
import {
  IoIosCheckmark,
  IoIosDownload,
  IoIosSearch,
  IoIosTrash,
} from 'react-icons/io';
import { Button } from '../../components/form';

const tr = styled.tr`
  border-collapse: separate;
  border-spacing: 1px 1px;
`;

const td = styled.td`
  padding: 0.7rem 1rem;
  white-space: nowrap;
  max-width: 200px;
  overflow: hidden;
  text-overflow: ellipsis;
  background: var(--white);
`;

const ListItem = ({
  _id,
  name,
  template,
  vendor,
  created_at,
  exporting,
  handleSelectItem,
  handleExportItem,
  handleDeleteList,
}) => {
  const [isDeleting, setIsDeleting] = useState(false);

  return (
    <tr>
      <td className="px-6 py-4 whitespace-nowrap">{name}</td>
      <td className="px-6 py-4 whitespace-nowrap">{created_at}</td>
      <td className="px-6 py-4 whitespace-nowrap">{template?.name}</td>
      <td className="px-6 py-4 whitespace-nowrap">{vendor?.name}</td>
      <td className="px-6 py-4 whitespace-nowrap" align="right">
        {!isDeleting && (
          <div>
            <Button
              className="btn btn-outlined"
              onClick={() => handleSelectItem(_id)}
            >
              <IoIosSearch />
            </Button>
            <Button
              className="btn btn-outlined btn-danger"
              onClick={() => setIsDeleting(true)}
            >
              <IoIosTrash />
            </Button>
            <Button
              className={`btn ${exporting ? 'btn-green' : 'btn-outlined '} `}
              onClick={() => handleExportItem(_id)}
            >
              {!exporting ? <IoIosDownload /> : <IoIosCheckmark />}
            </Button>
          </div>
        )}
        {isDeleting && (
          <div>
            <Button
              className="btn btn-outlined"
              onClick={() => setIsDeleting(false)}
            >
              Cancel
            </Button>
            <Button
              className="btn btn-outlined btn-danger"
              onClick={() => handleDeleteList(_id, name)}
            >
              Confirm
            </Button>
          </div>
        )}
      </td>
    </tr>
  );
};

export default ListItem;
