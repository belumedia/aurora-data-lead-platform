import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button, FormControl, FormGroup, Radiobox } from '../form';

let validationObj = {};

const validate = (values) => {
  const errors = {};
  if (!values.listName) {
    errors.listName = 'Required';
  }

  return errors;
};

const validateListName = (value) => {
  const { listName } = validationObj;
  if (value !== listName) {
    return `List name is not matching with ${listName}`;
  }
};

const renderField = (field) => {
  return (
    <FormControl fullWidth error={field.meta.touched && field.meta.error}>
      {field.label && <label htmlFor={field.id}>{field.label}</label>}
      <input {...field.input} type={field.type} />
      {field.meta.touched && field.meta.error && (
        <span className="error">{field.meta.error}</span>
      )}
    </FormControl>
  );
};

const renderRadioBox = (field) => {
  return (
    <>
      <Radiobox
        label={field.label}
        id={field.id}
        value={field.defaultValue}
        {...field.input}
      />
      {field.meta.touched && field.meta.error && (
        <span className="error">{field.meta.error}</span>
      )}
    </>
  );
};

const ListForm = ({
  handleSubmit,
  handleCancel,
  submitting,
  pristine,
  invalid,
  listName,
  vendors,
}) => {
  validationObj.listName = listName;

  return (
    <form onSubmit={handleSubmit}>
      <FormControl fullWidth>
        <label className="text-lg font-bold">List Name </label>
        <Field
          id="listName"
          name="listName"
          type="text"
          validate={validateListName}
          component={renderField}
        />
      </FormControl>
      {vendors.length > 0 && (
        <FormControl fullWidth>
          <label className="text-lg font-bold">Vendor </label>
          <FormGroup>
            {vendors.map((item) => (
              <Field
                id={item._id}
                label={item.name}
                defaultValue={item._id}
                name="vendor"
                component={renderRadioBox}
                key={item._id}
              />
            ))}
          </FormGroup>
        </FormControl>
      )}

      <FormControl>
        <Button type="submit" disabled={submitting || pristine || invalid}>
          Save
        </Button>
        <Button
          className="bg-transparent border border-green-500 mt-3 w-full inline-flex justify-center rounded-md shadow-sm px-4 py-2 text-base font-medium hover:bg-gray-50 hover:text-green-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 sm:mt-0 sm:ml-3 sm:w-auto"
          type="button"
          onClick={handleCancel}
        >
          Cancel
        </Button>
      </FormControl>
    </form>
  );
};

export default reduxForm({ form: 'ListForm', validate })(ListForm);
