import React from 'react';
import PropTypes from 'prop-types';
import ListItem from './ListItem';

function List({
  rows,
  exports,
  handleDeleteList,
  handleSelectItem,
  handleExportItem,
}) {
  function isOnExporting(id) {
    return exports.some((element) => element === id);
  }

  if (rows.length > 0) {
    return (
      <div className="flex flex-col w-full">
        <div className="overflow-x-auto">
          <table className="min-w-full divide-y divide-gray-200">
            <thead className="bg-gray-50">
              <tr>
                <th
                  scope="col"
                  className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                >
                  Name
                </th>
                <th
                  scope="col"
                  className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                >
                  Created At
                </th>
                <th
                  scope="col"
                  className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                >
                  Template
                </th>
                <th
                  scope="col"
                  className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                >
                  Vendor
                </th>
                <th
                  scope="col"
                  className="px-6 py-3 text-left text-sm font-medium text-gray-500 uppercase tracking-wider"
                ></th>
              </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
              {rows.map(({ _id, name, template, vendor, created_at }) => (
                <ListItem
                  _id={_id}
                  name={name}
                  template={template}
                  vendor={vendor}
                  created_at={created_at}
                  key={_id}
                  exporting={isOnExporting(_id)}
                  handleDeleteList={handleDeleteList}
                  handleSelectItem={handleSelectItem}
                  handleExportItem={handleExportItem}
                />
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  } else {
    return <h3 className="empty-list font-bold text-2xl">No list found...</h3>;
  }
}

List.propTypes = {
  rows: PropTypes.array.isRequired,
  offset: PropTypes.number.isRequired,
  limit: PropTypes.number.isRequired,
};

export default List;
