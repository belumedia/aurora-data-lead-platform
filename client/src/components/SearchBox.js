import React, { useState } from 'react';
import { IoIosSearch } from 'react-icons/io';
import { IconButton } from './form';

const SearchBox = ({ placeholder, onSubmit }) => {

    const [name, setName] = useState('');

    const onNameChange = e => {
        setName(e.target.value);
    }

    const onSearch = () => {
        onSubmit(name)
    }

    return (
        <div className="search">
            <input name="search" type="search" placeholder={placeholder} onChange={onNameChange} />
            {name &&
                <IconButton onClick={onSearch} disabled={name === ''}>
                    <IoIosSearch />
                </IconButton>
            }
        </div>
    );
};

SearchBox.defaultProps = {
    placeholder: 'Search...'
}

export default SearchBox;