import React, { useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import styled from 'styled-components';
import Main from '../components/Main';
import Toolbar from '../components/Toolbar';
import Block from '../components/Block';
import ContentWrapper from '../components/ContentWrapper';
import { updateProfile, updateEmailAddress, updatePassword, resetProfile } from '../services/authentication.service';
import EmailForm from '../components/auth/EmailForm';
import ProfileForm from '../components/auth/ProfileForm';
import PasswordForm from '../components/auth/PasswordForm';
import { useToasts } from 'react-toast-notifications';
import { IoIosLock, IoIosPerson, IoIosCreate } from 'react-icons/io';


const Grid = styled.div`
    display: grid;
    grid-auto-rows: auto;
    grid-template-columns: 1fr 1fr;
    grid-gap: 2rem;
    padding: 2rem;
`;

const GridItem = styled.div``;


const EditUserIcon = styled.div`
    background-color: var(--primary-green);
    padding: 1rem;
    border-radius: var(--border-radius-large);
    color: var(--white);
    font-size: 2rem;
    width: 60px;
    height: 60px;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
`;



const Profile = ({ auth: { user }, updateProfile, updateEmailAddress, updatePassword }) => {

    const [editEmail, setEditEmail] = useState(false);
    const toast = useToasts();

    const handleUpdateProfile = values => {

        updateProfile(values).then(res => {
            if (res.success) {
                toast.addToast(res.message, {
                    autoDismiss: true,
                    appearance: "success"
                })
            }
            else if (!res.success) {
                toast.addToast(res.error, {
                    autoDismiss: true,
                    appearance: "error"
                })
            }
        });

    }

    const handleUpdatePassword = values => {

        updatePassword(values).then(res => {
            if (res.success) {
                toast.addToast(res.message, {
                    autoDismiss: true,
                    appearance: "success"
                })
            }
            else if (!res.success) {
                toast.addToast(res.error, {
                    autoDismiss: true,
                    appearance: "error"
                })
            }
        });

    }

    const handleUpdateEmail = values => {

        updateEmailAddress(values).then(res => {
            if (res.success) {
                toast.addToast(res.message, {
                    autoDismiss: true,
                    appearance: "success"
                }, () => setEditEmail(false))
            }
            else if (!res.success) {
                toast.addToast(res.error, {
                    autoDismiss: true,
                    appearance: "error"
                }, () => setEditEmail(false))
            }
        });

    }

    const handleEditEmail = () => {
        setEditEmail(!editEmail)
    }

    return (
        <Main>
            <Toolbar title="Profile"></Toolbar>
            <ContentWrapper>

                <Grid>
                    <GridItem>

                        <Block
                            title={`${user.firstname || ''} ${user.lastname || ''}`}
                            description={user.email}
                            options={<EditUserIcon onClick={handleEditEmail}><IoIosCreate /></EditUserIcon>}
                        >
                            {editEmail &&
                                <EmailForm
                                    onSubmit={values => handleUpdateEmail(values)}
                                />
                            }
                        </Block>

                        <Block
                            title="Change Password"
                            description="Please choose a password which is longer than 6 characters"
                            icon={<IoIosLock />}
                        >
                            <PasswordForm
                                onSubmit={values => handleUpdatePassword(values)}
                            />
                        </Block>

                    </GridItem>

                    <GridItem>

                        <Block
                            title="User Informations"
                            description="Optional but would appreciate if you fill out this form"
                            icon={<IoIosPerson />}>
                            <ProfileForm
                                initialValues={{ firstname: user.firstname, lastname: user.lastname, phone: user.phone }}
                                onSubmit={values => handleUpdateProfile(values)}
                                profileInfo={user}
                            />
                        </Block>

                    </GridItem>

                </Grid>

            </ContentWrapper>

        </Main>
    );
};

const mapStateToProps = (state) => {
    return {
        form: state.form,
        auth: state.auth
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ updateProfile, resetProfile, updateEmailAddress, updatePassword }, dispatch)
}

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(Profile);