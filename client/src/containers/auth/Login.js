import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { useToasts } from 'react-toast-notifications';
import { load } from 'recaptcha-v3';
import { authenticate } from '../../services/authentication.service';
import logo from '../../assets/img/aurora.png';
import Loading from '../../components/Loading';
import LoginForm from '../../components/auth/LoginForm';

const Login = (props) => {
  const { isDispatching } = props.auth;
  const toast = useToasts();

  useEffect(() => {
    if (props.auth.isLogged) {
    }
  }, [props.auth.isLogged]);

  const onAuthenticate = async (values) => {
    const recaptcha = await load(process.env.REACT_APP_KEY_RECAPTCHA_PUBLIC);
    const g_token = await recaptcha.execute('login');
    const data = { ...values, g_token };

    props.authenticate(data).then((res) => {
      if (res.success) {
        props.history.push('/');
      } else if (!res.success) {
        toast.addToast(res.error, {
          appearance: 'error',
          autoDismiss: true,
        });
      }
    });
  };

  return (
    <div className="flex flex-row h-screen">
      <div className="bg-white px-20 flex items-end justify-center flex-col w-1/2 sm:w-full">
        <div className="max-w-lg w-full">
          <div className="w-16 mb-8">
            <img className="logo" src={logo} />
          </div>
          <h3 className="font-black text-4xl mb-8">Sign In</h3>
          <LoginForm onSubmit={onAuthenticate} />
        </div>
      </div>
      <div className="bg-green-700 flex justify-center flex-col w-1/2 sm:w-full">
        <svg
          viewBox="0 0 1531 891"
          preserveAspectRatio="xMidYMax slice"
          xmlns="http://www.w3.org/2000/svg"
          className="opacity-10 h-full"
        >
          <path d="M0 182c144.327 47.973 534.462 219.712 440.509 369.87C346.555 702.028 79.877 662.846 0 638V182z"></path>
          <path d="M1405 848c-424.366 158.009-437-164-437-272s102-425 563-576v769c-21.333 29.333-63.333 55.667-126 79z"></path>
          <path d="M1531 162c-122.914-17.284-377.96 33.191-543.433 206.414C822.095 541.636 797.342 648.75 835.842 731.622c38.5 82.871 198.243 134.841 400.555 92.053C1438.71 780.886 1492.752 775.894 1531 768V162z"></path>
        </svg>
      </div>
      <Loading showing={isDispatching} />
    </div>
  );
};

const mapStateToProps = (state) => ({ auth: state.auth });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ authenticate }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
