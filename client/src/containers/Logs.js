import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import Modal from 'react-modal';
import Pagination from 'react-paginate';
import { fetchLogs, fetchLog } from '../services/logs.service';
import Main from '../components/Main';
import Toolbar from '../components/Toolbar';
import ContentWrapper from '../components/ContentWrapper';
import List from '../components/logs/List';
import ExportList from '../components/logs/ExportList';
import Loading from '../components/Loading';
import {
  IoIosArrowForward,
  IoIosArrowBack,
  IoMdDownload,
} from 'react-icons/io';
import { CSVDownload, CSVLink } from 'react-csv';

class Logs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      limit: 100,
      modalOpen: false,
    };
  }

  componentDidMount() {
    this.fetchLogs();
  }

  fetchLogs = () => {
    const { offset, limit } = this.state;
    this.props.fetchLogs(offset, limit);
  };

  handleFetchLog = (id) => {
    this.props.fetchLog(id).then((res) => {
      if (res.success) {
        this.setState({ modalOpen: true });
      }
    });
  };

  handlePageChange = ({ selected }) => {
    const { limit } = this.state;
    this.setState(
      {
        offset: selected * limit,
      },
      () => {
        this.fetchLogs();
      }
    );
  };

  onCloseModal = () => {
    this.setState({
      modalOpen: false,
    });
  };

  render() {
    const { offset, limit, modalOpen } = this.state;
    const { logs } = this.props;

    console.log(logs.exportsLog);

    return (
      <Main>
        <Toolbar title="Logs" />

        <ContentWrapper>
          <List
            rows={logs.items}
            offset={offset}
            limit={limit}
            count={logs.totalCount}
            handleFetchLog={this.handleFetchLog}
            handlePageChange={this.handlePageChange}
          />

          {logs.totalCount > limit && (
            <Pagination
              containerClassName="pagination"
              previousLabel={<IoIosArrowBack />}
              nextLabel={<IoIosArrowForward />}
              onPageChange={this.handlePageChange}
              pageCount={logs.totalCount / limit}
            />
          )}
        </ContentWrapper>

        <Loading showing={logs.isDispatching} />

        <Modal
          isOpen={modalOpen}
          className="absolute align-bottom bg-white border inline-block overflow-hidden outline-none right-1/2 rounded-lg shadow-xl sm:align-middle sm:max-w-7xl sm:my-8 sm:w-full text-left top-1/2 transform transition-all duration-500 translate-x-1/2 -translate-y-1/2"
        >
          <div className="bg-white max-h-96 overflow-y-auto">
            <div className="flex justify-between px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
              <h3 className="text-lg leading-6 font-medium text-gray-900">
                Export status details
              </h3>
              <span>
                {logs?.exportsLog?.list?.contacts &&
                  logs?.exportsLog?.details?.list?.name && (
                    <CSVLink
                      data={logs.exportsLog.list.contacts}
                      filename={`Log-${logs.exportsLog.details.list.name}`}
                      className="mt-3 w-full inline-flex items-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                    >
                      Download <IoMdDownload class="ml-2" />
                    </CSVLink>
                  )}
              </span>
            </div>
            <div className="mt-2">
              <ExportList
                rows={logs.exportsLog.list.contacts}
                headers={logs.exportsLog.list.headers}
              />
            </div>
          </div>
          <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
            <button
              onClick={this.onCloseModal}
              type="button"
              className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
            >
              Close
            </button>
          </div>
        </Modal>
      </Main>
    );
  }
}

function mapStateToProps(state) {
  return {
    logs: state.logs,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchLogs, fetchLog }, dispatch);
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter
)(Logs);
