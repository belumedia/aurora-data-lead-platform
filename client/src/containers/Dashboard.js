import React, { useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/Main';
import Toolbar from '../components/Toolbar';
import List from '../components/logs/List';
import ExportList from '../components/logs/ExportList';
import { fetchStats } from '../services/dashboard.service';
import { fetchLog } from '../services/logs.service';
import { greatings } from '../helpers/user';
import CircularSpinner from '../components/CircularSpinner';
import ReactModal from 'react-modal';
import { CSVLink } from 'react-csv';
import { IoMdDownload } from 'react-icons/io';

const Dashboard = ({ fetchStats, fetchLog, dashboard, logs }) => {
  const [modalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    fetchStats();
  }, []);

  function handleFetchLog(id) {
    fetchLog(id).then((res) => {
      if (res.success) {
        setModalOpen(() => true);
      }
    });
  }

  function onCloseModal() {
    setModalOpen(() => false);
  }

  return (
    <Main>
      <Toolbar title="Dashboard" />
      <div className="flex flex-col flex-auto w-full p-8 xs:p-2">
        <div className="flex flex-wrap w-full">
          <div className="flex items-center justify-between w-full my-4 px-4 xs:pr-0">
            <div className="mr-6">
              <h2 className="m-0 font-bold text-4xl">{greatings()}</h2>
              <div className="text-gray-400 tracking-tight">
                Keep track of your data status
              </div>
            </div>
          </div>
          <div className="flex items-center w-full my-8">
            <div className="w-full grid gap-7 sm:grid-cols-2 lg:grid-cols-4">
              <div className="p-5 bg-white rounded shadow-sm relative">
                {dashboard.isDispatching && <CircularSpinner />}
                <div className="text-base text-gray-400 ">Total Contacts</div>
                <div className="flex items-center pt-1">
                  <div className="text-2xl font-bold text-gray-900">
                    {dashboard?.stats?.contacts}
                  </div>
                </div>
              </div>
              <div className="p-5 bg-white rounded shadow-sm relative">
                {dashboard.isDispatching && <CircularSpinner />}
                <div className="text-base text-gray-400 ">Campaigns</div>
                <div className="flex items-center pt-1">
                  <div className="text-2xl font-bold text-gray-900">
                    {dashboard?.stats?.lists}
                  </div>
                </div>
              </div>
              <div className="p-5 bg-white rounded shadow-sm relative">
                {dashboard.isDispatching && <CircularSpinner />}
                <div className="text-base text-gray-400 ">Vendors</div>
                <div className="flex items-center pt-1">
                  <div className="text-2xl font-bold text-gray-900">
                    {dashboard?.stats?.vendors}
                  </div>
                </div>
              </div>
              <div className="p-5 bg-white rounded shadow-sm relative">
                {dashboard.isDispatching && <CircularSpinner />}
                <div className="text-base text-gray-400 ">Templates</div>
                <div className="flex items-center pt-1">
                  <div className="text-2xl font-bold text-gray-900">
                    {dashboard?.stats?.templates}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-wrap w-full">
          <h3 className="font-bold my-4 text-xl">Last exports logs</h3>

          <List
            rows={dashboard?.stats?.exports || []}
            handleFetchLog={handleFetchLog}
          />

          <ReactModal
            isOpen={modalOpen}
            className="absolute align-bottom bg-white border inline-block overflow-hidden outline-none right-1/2 rounded-lg shadow-xl sm:align-middle sm:max-w-7xl sm:my-8 sm:w-full text-left top-1/2 transform transition-all duration-500 translate-x-1/2 -translate-y-1/2"
          >
            <div className="bg-white max-h-96 overflow-y-auto">
              <div className="flex justify-between px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <h3 className="text-lg leading-6 font-medium text-gray-900">
                  Export status details
                </h3>
                <span>
                  {logs?.exportsLog?.list?.contacts &&
                    logs?.exportsLog?.details?.list?.name && (
                      <CSVLink
                        data={logs.exportsLog.list.contacts}
                        filename={`Log-${logs.exportsLog.details.list.name}`}
                        className="mt-3 w-full inline-flex items-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                      >
                        Download <IoMdDownload class="ml-2" />
                      </CSVLink>
                    )}
                </span>
              </div>
              <div className="mt-2">
                <ExportList
                  rows={logs.exportsLog.list.contacts}
                  headers={logs.exportsLog.list.headers}
                />
              </div>
            </div>
            <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
              <button
                onClick={onCloseModal}
                type="button"
                className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
              >
                Close
              </button>
            </div>
          </ReactModal>
        </div>
      </div>
    </Main>
  );
};

function mapStateToProps(state) {
  return {
    dashboard: state.stats,
    logs: state.logs,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchStats,
      fetchLog,
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
