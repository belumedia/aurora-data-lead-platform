import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { useToasts } from 'react-toast-notifications';
import styled from 'styled-components';
import Main from '../../components/Main';
import Toolbar from '../../components/Toolbar';
import ContentWrapper from '../../components/ContentWrapper';
import UserForm from '../../components/user/UserForm'
import Block from '../../components/Block';
import { IoIosPerson } from 'react-icons/io';
import { fetchUser, updateUser } from '../../services/users.service';
import { fetchVendors } from '../../services/vendor.service';

const Grid = styled.div`
    padding: 2rem;
    display: grid;
    grid-auto-rows: auto;
    grid-template-columns: 1fr 1fr;
    grid-gap: 2rem;
`;

const GridItem = styled.div``;

const EditUser = ({ users: { userInfo }, vendors, fetchUser, fetchVendors, updateUser, history, match }) => {

    const { addToast } = useToasts();

    useEffect(() => {
        const { id } = match.params;
        fetchUser(id);
        fetchVendors();
    }, [])


    const handleSubmit = (values) => {

        const { id } = match.params;
        const data = { ...values };

        if (values.vendors) {
            data.vendors = values.vendors.reduce((filteredVendors, item) => {
                filteredVendors.push(item.value)
                return filteredVendors;
            }, []);
        }

        updateUser(id, data).then(res => {
            if (res.success) {
                addToast(res.message, {
                    appearance: 'success',
                    autoDismiss: true
                }, () => history.goBack())
            }
            else if (!res.success) {
                addToast(res.error, {
                    appearance: 'error',
                    autoDismiss: true
                })
            }
        })
    }

    const getOptions = (userVendors, vendors) => {
        
        return vendors.reduce((selectedVendors, val) => {
            if (userVendors?.includes(val._id)) {
                selectedVendors.push({ value: val._id, label: val.name })
            }
            return selectedVendors;
        }, [])
    }



    return (
        <Main>
            <Toolbar title="Edit User" goBack={() => history.goBack()} />
            <ContentWrapper>
                <Grid>
                    <GridItem>
                        <Block
                            title="User Informations"
                            icon={<IoIosPerson />}
                            description="Add the User informations and the role that will cover on Aurora, can you assign also the Vendor that belong to"
                        >
                            <UserForm
                                initialValues={{
                                    firstname: userInfo.firstname,
                                    lastname: userInfo.lastname,
                                    email: userInfo.email,
                                    username: userInfo.username,
                                    phone: userInfo.phone,
                                    is_active: userInfo.is_active,
                                    admin: userInfo.role === 'admin',
                                    vendors: getOptions(userInfo.vendors, vendors.list)
                                }}
                                vendors={vendors.list}
                                onSubmit={handleSubmit} />

                        </Block>
                    </GridItem>
                </Grid>
            </ContentWrapper>
        </Main>
    );
};



function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchUser, fetchVendors, updateUser }, dispatch)
}

function mapStateToProps(state) {
    return {
        users: state.users,
        vendors: state.vendors
    }
}

export default compose(connect(mapStateToProps, mapDispatchToProps))(EditUser);