import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { useToasts } from 'react-toast-notifications';
import styled from 'styled-components';
import Main from '../../components/Main';
import Toolbar from '../../components/Toolbar';
import ContentWrapper from '../../components/ContentWrapper';
import TemplateForm from '../../components/template/TemplateForm'
import Block from '../../components/Block';
import { IoIosArchive } from 'react-icons/io';
import { createTemplate } from '../../services/import.service';
import { fieldsMapperList } from '../../constants/fieldmapper';

const Grid = styled.div`
    padding: 2rem;
    display: grid;
    grid-auto-rows: auto;
    grid-template-columns: 1fr 1fr;
    grid-gap: 2rem;
`;

const GridItem = styled.div``;

const AddTemplate = ({ createTemplate, history }) => {

    const { addToast } = useToasts();

    const handleSubmit = (values) => {
        createTemplate(values).then(res => {
            if (res.success) {
                addToast('Template added succesfully', {
                    appearance: 'success',
                    autoDismiss: true
                }, () => history.goBack())
            }
            else if (!res.success) {
                addToast(res.error, {
                    appearance: 'error',
                    autoDismiss: true
                })
            }
        })
    }

    return (
        <Main>
            <Toolbar title="Add a new Template" goBack={ () => history.push('/templates') } />
            <ContentWrapper>
                <Grid>
                    <GridItem>
                        <Block
                            title="Template Informations"
                            icon={<IoIosArchive />}
                            description="Add the template fields in pair Label that which will show the Column name and the Value which is a reference on the Database, you can also decide which has to be filterable on the search page."
                        >
                            <TemplateForm onSubmit={handleSubmit} />
                        </Block>
                    </GridItem>
                </Grid>
            </ContentWrapper>
        </Main>
    );
};



function mapDispatchToProps(dispatch) {
    return bindActionCreators({ createTemplate }, dispatch)
}

export default compose(connect(null, mapDispatchToProps))(AddTemplate);