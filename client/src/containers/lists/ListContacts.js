import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withToastManager } from 'react-toast-notifications';
import Pagination from 'react-paginate';
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io';
import {
  getContacts,
  getListContacts,
  exportContactsToURL,
  deleteContact,
  selectContact,
  selectFilter,
} from '../../services/contacts.service';
import Main from '../../components/Main';
import ContentWrapper from '../../components/ContentWrapper';
import Toolbar from '../../components/Toolbar';
import List from '../../components/contacts/List';
import FiltersMenu from '../../components/contacts/FiltersMenu';
import ExportMenu from '../../components/contacts/ExportMenu';
import Loading from '../../components/Loading';
import { Button } from '../../components/form';

class ListContacts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFiltering: false,
      isExporting: false,
      offset: 0,
      limit: 50,
    };
  }

  componentDidMount() {
    this.fetchContacts();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.contacts.listSent !== this.props.contacts.listSent) {
      this.setState({
        isFiltering: false,
        isExporting: false,
      });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.match.params.id !== this.props.match.params.id ||
      nextProps.contacts.isDispatching !== this.props.contacts.isDispatching ||
      nextProps.contacts.listSent !== this.props.contacts.listSent ||
      nextProps.contacts.selected.length !==
        this.props.contacts.selected.length ||
      nextState.isFiltering !== this.state.isFiltering ||
      nextState.isExporting !== this.state.isExporting ||
      nextState.offset !== this.state.offset
    );
  }

  fetchContacts = async () => {
    const { toastManager } = this.props;
    const { id } = this.props.match.params;
    if (id) {
      await this.props.getListContacts(id).then((res) => {
        if (res.error) {
          toastManager.add(res.error, {
            appearance: 'error',
            autoDismiss: true,
          });
        }
      });
    }
  };

  handleChangeFilter = (name, value) => {
    this.props.selectFilter(name, value);
  };

  handleDeleteContact = (id) => {
    this.props.deleteContact(id);
  };

  handleGetContacts = () => {
    const { id } = this.props.match.params;
    this.props.getListContacts(id);
  };

  handleGetFilteredContacts = (filters) => {
    const { id } = this.props.match.params;
    const filtersParam = JSON.stringify(filters);
    this.props.getListContacts(id, filtersParam);
  };

  handleSelectItem = (id) => {
    this.props.selectContact(id);
  };

  handleDeleteItem = (id) => {
    this.props.deleteContact(id);
  };

  handleFilter = () => {
    this.setState({
      isFiltering: !this.state.isFiltering,
    });
  };

  handleExport = () => {
    this.setState({
      isExporting: !this.state.isExporting,
    });
  };

  handlePageChange = ({ selected }) => {
    const { limit } = this.state;
    this.setState({
      offset: selected * limit,
    });
  };

  handleExportContactsToURL = (contacts, list, url) => {
    const { toastManager } = this.props;

    this.props.exportContactsToURL(contacts, list, url).then((res) => {
      if (res.success) {
        toastManager.add(res.message, {
          appearance: 'success',
          autoDismiss: true,
        });
      } else if (res.error) {
        toastManager.add(res.error, {
          appearance: 'error',
          autoDismiss: true,
        });
      }
    });
  };

  render() {
    const { contacts } = this.props;
    const { isFiltering, isExporting, offset, limit } = this.state;
    const headers =
      (contacts.headers &&
        contacts.headers.template &&
        contacts.headers.template.fields) ||
      [];
    const title = (contacts.list && contacts.list.name) || null;
    // fix bugs on title name when is calling like contacts.list

    return (
      <Main>
        <Toolbar title={title} goBack={() => this.props.history.push('/lists')}>
          {contacts.selected.length > 0 && (
            <span className="text-danger mr-1">
              Contacts selected{' '}
              <strong className="badge"> {contacts.selected.length} </strong>
            </span>
          )}
          {contacts.items.length > 0 && (
            <>
              <Button onClick={this.handleExport}>Export Contacts</Button>
              <Button onClick={this.handleFilter}>Filter Contacts</Button>
            </>
          )}
        </Toolbar>
        <ContentWrapper>
          <List
            handleSelectItem={this.handleSelectItem}
            handleDeleteItem={this.handleDeleteItem}
            handlePageChange={this.handlePageChange}
            headers={headers}
            rows={contacts.items.slice(offset, offset + limit)}
            selected={contacts.selected}
          />

          {contacts.totalCount > limit && (
            <Pagination
              containerClassName="pagination"
              previousLabel={<IoIosArrowBack />}
              nextLabel={<IoIosArrowForward />}
              initialPage={offset}
              onPageChange={this.handlePageChange}
              pageRangeDisplayed={5}
              marginPagesDisplayed={2}
              pageCount={contacts.totalCount / limit}
            />
          )}

          <FiltersMenu
            open={isFiltering}
            filtersList={headers}
            handleFilter={this.handleFilter}
            handleGetContacts={this.handleGetContacts}
            handleGetFilteredContacts={this.handleGetFilteredContacts}
            onChange={this.handleChangeFilter}
            defaultLabel="Filter Results"
          />

          <ExportMenu
            open={isExporting}
            headers={headers}
            list={contacts.list}
            data={contacts.items}
            selected={contacts.selected}
            handleExport={this.handleExport}
            handleExportContactsToURL={this.handleExportContactsToURL}
            onExport={this.handleChangeFilter}
            defaultLabel="Export Results"
          />
        </ContentWrapper>
        <Loading showing={contacts.isDispatching} />
      </Main>
    );
  }
}

function mapStateToProps(state) {
  return {
    contacts: state.contacts,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getContacts,
      getListContacts,
      exportContactsToURL,
      deleteContact,
      selectContact,
      selectFilter,
    },
    dispatch
  );
}

export default compose(
  withToastManager,
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(ListContacts);
