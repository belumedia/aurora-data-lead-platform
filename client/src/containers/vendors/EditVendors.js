import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { useToasts } from 'react-toast-notifications';
import styled from 'styled-components';
import Main from '../../components/Main';
import Toolbar from '../../components/Toolbar';
import ContentWrapper from '../../components/ContentWrapper';
import VendorEditForm from '../../components/vendors/VendorEditForm'
import Block from '../../components/Block';
import { IoIosArchive } from 'react-icons/io';
import { updateVendor, fetchVendor } from '../../services/vendor.service';
import Loading from '../../components/Loading';

const Grid = styled.div`
    padding: 2rem;
    display: grid;
    grid-auto-rows: auto;
    grid-template-columns: 1fr 1fr;
    grid-gap: 2rem;
`;

const GridItem = styled.div``;

const EditVendors = ({ vendors: { vendor, isDispatching, error, success }, fetchVendor, updateVendor, history, match }) => {

    const { addToast } = useToasts();

    useEffect(() => {
        const { id } = match.params;
        fetchVendor(id);
    }, []);

    const handleSubmnit = (values) => {
        const { id } = match.params;
        const data = new FormData();
 
        for(let value in values){
            if (value === 'logo' && value !== '') {
                data.append(value, values[value][0])
            }
            else {
                data.set(value, values[value])
            }
        }

        updateVendor(id, data).then(res => {
            if (res.success) {
                addToast('Vendor Updated',{
                    appearance: 'success',
                    autoDismiss: true
                }, () => history.goBack())
            }
            else if(!res.success){
                addToast(res.error,{
                    appearance: 'error',
                    autoDismiss: true
                })
            }
        })

    }

    return (
        <Main>
            <Toolbar title="Edit a the Vendor" goBack={ () => history.push('/vendors') } />
            <ContentWrapper>
                <Grid>
                    <GridItem>
                        <Block
                            title="Vendor Informations"
                            icon={<IoIosArchive />}
                            description="Insert all informations, such name, website, and the company logo">

                            <VendorEditForm 
                                initialValues={{ 
                                    name: vendor.name 
                                }} 
                                onSubmit={handleSubmnit} 
                            />

                            <Loading showing={isDispatching} />

                        </Block>
                    </GridItem>
                </Grid>
            </ContentWrapper>
        </Main>
    );
};


function mapStateToProps(state) {
    return {
        vendors: state.vendors
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchVendor, updateVendor }, dispatch)
}

export default compose(connect(mapStateToProps, mapDispatchToProps))(EditVendors);