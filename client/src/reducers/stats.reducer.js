import {
  REQUEST_STATS,
  GET_STATS,
  GET_STATS_FAILS,
} from '../actions/actionTypes';

const initialState = {
  stats: {},
  isDispatching: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case REQUEST_STATS:
      return {
        ...state,
        isDispatching: true,
      };

    case GET_STATS:
      return {
        ...state,
        stats: action.payload.stats,
        isDispatching: false,
      };

    case GET_STATS_FAILS:
      return {
        ...state,
        error: action.payload.error,
        isDispatching: false,
      };

    default:
      return state;
  }
}
