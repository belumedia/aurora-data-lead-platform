import { REQUEST_USERS, GET_USERS, GET_USERS_FAILS, GET_USER, GET_USER_FAILS, DELETE_USER, DELETE_USER_FAILS } from '../actions/actionTypes';

const defaultState = {
    list: [],
    userInfo: {},
    isDispatching: false
}

export default function (state = defaultState, action) {

    switch (action.type) {

        case REQUEST_USERS:

            return {
                ...state,
                isDispatching: true
            }

        case GET_USERS:
            return {
                ...state,
                list: action.payload.users,
                isDispatching: false
            }

        case GET_USERS_FAILS:
            return {
                ...state,
                isDispatching: false
            }


        case GET_USER:

            return {
                ...state,
                userInfo: action.payload.user,
                isDispatching: false
            }

        case GET_USER_FAILS:

            return {
                ...state,
                isDispatching: false
            }

        case DELETE_USER:

            return {
                ...state,
                list: state.list.filter(user => user._id !== action.payload.user._id),
                isDispatching: false
            }


        case DELETE_USER_FAILS:

            return {
                ...state,
                isDispatching: false
            }

        default:
            return state
    }

}