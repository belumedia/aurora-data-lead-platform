import {
  REQUEST_LOGS,
  GET_LOGS,
  GET_LOGS_FAILS,
  GET_LOG,
  GET_LOG_FAILS,
} from '../actions/actionTypes';

const defaultState = {
  items: [],
  totalCount: 0,
  exportsLog: {
    details: {},
    list: {
      headers: [],
      contacts: [],
    },
  },
  isDispatching: false,
  success: false,
  error: false,
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case REQUEST_LOGS:
      return {
        ...state,
        isDispatching: true,
      };

    case GET_LOGS:
      return {
        ...state,
        items: action.payload.logs.docs,
        totalCount: action.payload.logs.totalDocs,
        isDispatching: false,
      };

    case GET_LOGS_FAILS:
      return {
        ...state,
        items: [],
        totalCount: 0,
        isDispatching: false,
        success: false,
        error: action.payload.error,
      };

    case GET_LOG:
      return {
        ...state,
        exportsLog: {
          details: action.payload.details,
          list: {
            headers: action.payload.list.headers,
            contacts: action.payload.list.contacts,
          },
        },
        isDispatching: false,
      };

    case GET_LOG_FAILS:
      return {
        ...state,
        details: [],
        isDispatching: false,
        success: false,
        error: action.payload.error,
      };

    default:
      return state;
  }
};
