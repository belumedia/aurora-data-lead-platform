import * as types from '../actions/actionTypes';
import axios from 'axios';

export const fetchLogs = (offset, limit) => async (dispatch) => {
  try {
    dispatch({ type: types.REQUEST_LOGS });
    const url = `/logs?offset=${offset}&limit=${limit}`;
    const res = await axios.get(url);
    dispatch({ type: types.GET_LOGS, payload: res.data });
  } catch (error) {
    dispatch({ type: types.GET_LOGS_FAILS, payload: error.response.data });
  }
};

export const fetchLog = (id) => async (dispatch) => {
  try {
    dispatch({ type: types.REQUEST_LOGS });
    const url = `/logs/${id}`;
    const res = await axios.get(url);
    dispatch({ type: types.GET_LOG, payload: res.data });
    return res.data;
  } catch (error) {
    dispatch({ type: types.GET_LOG_FAILS, payload: error.response.data });
  }
};
