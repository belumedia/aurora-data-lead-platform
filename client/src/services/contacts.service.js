import * as types from '../actions/actionTypes';
import axios from 'axios';

export const getContacts = (offset, limit) => async (dispatch) => {
  try {
    dispatch({ type: types.REQUEST_CONTACTS });
    const url = `/contacts?offset=${offset}&limit=${limit}`;
    const res = await axios.get(url);
    dispatch({ type: types.GET_CONTACTS, payload: res.data });
    return res.data;
  } catch (error) {
    dispatch({
      type: types.GET_CONTACTS_FAILS,
      payload: { error: error.response.data },
    });
    return error.response.data;
  }
};

export const getListContacts = (id, filters) => async (dispatch) => {
  try {
    dispatch({ type: types.REQUEST_CONTACTS });
    const url = !filters
      ? `/lists/id/${id}`
      : `/lists/id/${id}?filters=${filters}`;
    const res = await axios.get(url);
    dispatch({ type: types.GET_CONTACTS, payload: res.data });
    return res.data;
  } catch (error) {
    dispatch({
      type: types.GET_CONTACTS_FAILS,
      payload: { error: error.response.data },
    });
    return error.response.data;
  }
};

export const exportContactsToExternalURL = (contacts, toURL) => async (
  dispatch
) => {
  try {
    dispatch({ type: types.REQUEST_CONTACTS });
    let axiosArray = [];
    contacts.forEach((contact) => {
      let newPromise = axios.post(toURL, contact);
      axiosArray.push(newPromise);
    });
    const requests = await axios.all(axiosArray);
    const report = requests.map((request, idx) =>
      request.status === 200
        ? { contact: contacts[idx], success: true }
        : { contact: contacts[idx], success: false }
    );

    dispatch({ type: types.SEND_CONTACTS, payload: { report } });
  } catch (error) {
    dispatch({
      type: types.SEND_CONTACTS_FAILS,
      payload: { error: error.response.data },
    });
  }
};

export const exportContactsToURL = (contacts, list, url) => async (
  dispatch
) => {
  try {
    dispatch({ type: types.REQUEST_CONTACTS });
    const res = await axios.post(`/contacts/export`, { url, list, contacts });
    dispatch({ type: types.SEND_CONTACTS, payload: res.data });
    return res.data;
  } catch (error) {
    dispatch({
      type: types.SEND_CONTACTS_FAILS,
      payload: { error: error.response.data },
    });
    return error.response.data;
  }
};

export const resetContacts = () => async (dispatch) => {
  dispatch({ type: types.RESET_CONTACTS_LIST });
  return true;
};

export const handleContactsImported = (items) => async (dispatch) => {
  await dispatch({ type: types.GET_CONTACTS_IMPORTED, payload: { items } });
};

export const saveContacts = (items) => async (dispatch) => {
  try {
    dispatch({ type: types.REQUEST_IMPORT });
    const url = `/contacts/insert/bulk`;
    const res = await axios.post(url, { items });
    dispatch({ type: types.SAVE_CONTACTS, payload: res.data });
  } catch (error) {
    dispatch({
      type: types.SAVE_CONTACTS_FAILS,
      payload: { error: error.response.data },
    });
  }
};

export const deleteContact = (id) => async (dispatch) => {
  try {
    dispatch({ type: types.REQUEST_ITEMS });
    const url = `/contacts/${id}`;
    const res = await axios.delete(url);
    dispatch({ type: types.DELETE_CONTACT, payload: res.data });
  } catch (error) {
    dispatch({
      type: types.DELETE_CONTACT_FAILS,
      payload: { error: error.response.data },
    });
  }
};

export const selectContact = (id) => (dispatch) => {
  dispatch({ type: types.SELECT_CONTACT, payload: { id } });
};

export const selectFilter = (name, value) => (dispatch) => {
  dispatch({ type: types.FILTERING_ITEMS, payload: { name, value } });
};
