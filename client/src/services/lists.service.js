import * as types from '../actions/actionTypes';
import axios from 'axios';

import { dispatchingRequest, successCall, errorCall } from '../actions/index';

export const fetchLists = (offset, limit) => async (dispatch) => {
  try {
    dispatch(dispatchingRequest(types.REQUEST_ITEMS));
    const url = `/lists?offset=${offset}&limit=${limit}`;
    const res = await axios.get(url);
    dispatch(successCall(types.GET_LISTS, res.data));
  } catch (error) {
    dispatch(errorCall(types.GET_LISTS_FAILS, error.response.data));
  }
};

export const fetchListByName = (name, offset, limit) => async (dispatch) => {
  try {
    dispatch(dispatchingRequest(types.REQUEST_ITEMS));
    const url = `/lists?name=${name}&offset=${offset}&limit=${limit}`;
    const res = await axios.get(url);
    dispatch(successCall(types.GET_LISTS, res.data));
  } catch (error) {
    dispatch(errorCall(types.GET_LISTS_FAILS, error.response.data));
  }
};

export const fetchListByDates = (startDate, endDate, offset, limit) => async (
  dispatch
) => {
  try {
    dispatch(dispatchingRequest(types.REQUEST_ITEMS));
    const url = `/lists?startDate=${startDate}&endDate=${endDate}&offset=${offset}&limit=${limit}`;
    const res = await axios.get(url);
    dispatch(successCall(types.GET_LISTS, res.data));
  } catch (error) {
    dispatch(errorCall(types.GET_LISTS_FAILS, error.response.data));
  }
};

export const saveList = (name, items, template, vendor) => async (dispatch) => {
  try {
    dispatch(dispatchingRequest(types.REQUEST_IMPORT));
    const url = `/lists`;
    const res = await axios.post(url, { name, items, template, vendor });
    dispatch(successCall(types.SAVE_CONTACTS_LIST, res.data));
    return res.data;
  } catch (error) {
    console.log(`😱 Axios request failed: ${error}`);
    dispatch(errorCall(types.SAVE_CONTACTS_LIST_FAILS, error.response.data));
    return error.response.data;
  }
};

export const deleteList = (id) => async (dispatch) => {
  try {
    dispatch(dispatchingRequest(types.REQUEST_ITEMS));
    const url = `/lists/${id}`;
    const res = await axios.delete(url);
    dispatch(successCall(types.DELETE_ITEM, res.data));
    return res.data;
  } catch (error) {
    console.log(`😱 Axios request failed: ${error}`);
    dispatch(errorCall(types.DELETE_ITEM_FAILS, error.response.data));
    return error.response.data;
  }
};

export const addToExportItem = (id) => (dispatch) => {
  dispatch(successCall(types.ADD_TO_EXPORT, { id }));
};

export const exportLists = (items) => async (dispatch) => {
  try {
    dispatch(dispatchingRequest(types.REQUEST_ITEMS));
    const url = `/lists/export/`;
    const res = await axios.post(url, { items });
    dispatch(successCall(types.EXPORT_LISTS, res.data));
    return res.data;
  } catch (error) {
    console.log(`😱 Axios request failed: ${error}`);
    dispatch(errorCall(types.EXPORT_LISTS_FAILS, error.response.data));
    return error.response.data;
  }
};

export const resetListState = () => (dispatch) => {
  dispatch(dispatchingRequest(types.RESET_LIST_STATE));
};
