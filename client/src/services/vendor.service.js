import * as types from '../actions/actionTypes';
import axios from 'axios';

import { dispatchingRequest, successCall, errorCall } from '../actions/index';


export const fetchVendors = (page, limit) => async dispatch => {

    try {
        dispatch(dispatchingRequest(types.REQUEST_VENDORS));
        const url = `/vendors?page=${page}&limit=${limit}`;
        const res = await axios.get(url);
        dispatch(successCall(types.GET_VENDORS, res.data))
    } catch (error) {
        dispatch(errorCall(types.GET_VENDORS_FAILS, error.response.data))
    }

}

export const fetchVendor = id => async dispatch => {

    try {
        dispatch(dispatchingRequest(types.REQUEST_VENDORS));
        const url = `/vendors/id/${id}`;
        const res = await axios.get(url);
        dispatch(successCall(types.GET_VENDOR, res.data))
    } catch (error) {
        dispatch(errorCall(types.GET_VENDOR_FAILS, error.response.data))
    }

}

export const addVendor = ({ name }) => async dispatch => {

    try {
        dispatch(dispatchingRequest(types.REQUEST_VENDORS));
        const url = `/vendors`;
        const res = await axios.post(url, { name });
        dispatch(successCall(types.ADD_VENDOR, res.data))
        return res.data;
    } catch (error) {
        console.log(`😱 Axios request failed: ${error}`);
        dispatch(errorCall(types.ADD_VENDOR_FAILS, error.response.data))
        return error.response.data;
    }

}

export const updateVendor = (id, data) => async dispatch => {

    try {
        dispatch(dispatchingRequest(types.REQUEST_VENDORS));
        const url = `/vendors/${id}`;
        const res = await axios({url, method: 'post', data, 
            headers: {  'Content-Type': 'multipart/form-data'} 
        });
        dispatch(successCall(types.UPDATE_VENDOR, res.data))
        return res.data;
    } catch (error) {
        console.log(`😱 Axios request failed: ${error}`);
        dispatch(errorCall(types.UPDATE_VENDOR_FAILS, error.response.data));
        return error.response.data;
    }

}

export const deleteVendor = id => async dispatch => {

    try {
        dispatch(dispatchingRequest(types.REQUEST_VENDORS));
        const url = `/vendors/${id}`;
        const res = await axios.delete(url)
        dispatch(successCall(types.DELETE_VENDOR, res.data));
        return res.data;
    } catch (error) {
        console.log(`😱 Axios request failed: ${error}`);
        dispatch(errorCall(types.DELETE_VENDOR_FAILS, error.response.data));
        return error.response.data;
    }

}