import axios from 'axios';
import * as types from '../actions/actionTypes';

export const fetchStats = () => async (dispatch) => {
  try {
    dispatch({ type: types.REQUEST_STATS });
    const res = await axios.get('stats');
    dispatch({ type: types.GET_STATS, payload: res.data });
  } catch (error) {
    dispatch({ type: types.GET_STATS_FAILS, payload: error.response.data });
  }
};
